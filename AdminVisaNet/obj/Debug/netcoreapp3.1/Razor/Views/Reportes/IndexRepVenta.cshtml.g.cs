#pragma checksum "C:\Users\guill\Documents\umg\Seminario\POS\AdminVisaNet\Views\Reportes\IndexRepVenta.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4c39c2c006c62afa228d4c49a4f54b944844a04b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Reportes_IndexRepVenta), @"mvc.1.0.view", @"/Views/Reportes/IndexRepVenta.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\guill\Documents\umg\Seminario\POS\AdminVisaNet\Views\_ViewImports.cshtml"
using AdminVisaNet;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\guill\Documents\umg\Seminario\POS\AdminVisaNet\Views\_ViewImports.cshtml"
using AdminVisaNet.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\guill\Documents\umg\Seminario\POS\AdminVisaNet\Views\Reportes\IndexRepVenta.cshtml"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4c39c2c006c62afa228d4c49a4f54b944844a04b", @"/Views/Reportes/IndexRepVenta.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c645737a9af3543de74dfa52ac432518dc162c6e", @"/Views/_ViewImports.cshtml")]
    public class Views_Reportes_IndexRepVenta : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "0", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/jquery.mCustomScrollbar.concat.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/main.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
            WriteLiteral(@"
<section class=""full-width pageContent"">
    <section class=""full-width text-center"" style=""padding: 10px 0;"">
        <h3 class=""text-center tittles"">REPORTE VENTAS</h3>
    </section>
    <div class=""row"" style=""padding:1em 2em"">
        <div class=""col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"">
            <label class=""col-sm-6 col-form-label"">Establecimientos:</label>
            <select id=""ddlEstablecimiento"" class=""form-control"" onchange=""myFunction()"">
                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "4c39c2c006c62afa228d4c49a4f54b944844a04b5061", async() => {
                WriteLiteral("Seleciona un establecimiento");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            BeginWriteTagHelperAttribute();
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __tagHelperExecutionContext.AddHtmlAttribute("selected", Html.Raw(__tagHelperStringValueBuffer), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.Minimized);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@">
            </select>
        </div>
        <div class=""col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"">
            <label class=""col-sm-6 col-form-label"">Corte:</label>
            <input type=""text"" class=""form-control"" id=""txtIDCorte"" placeholder=""Ingrese IDCorte"">
        </div>

    </div>
    <div class=""row"" style=""padding:1em 2em"">
        <div class=""col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"">
            <label class=""col-sm-6 col-form-label"">Fecha inicial:</label>
            <input type=""text"" class=""form-control"" id=""txtFechaIni"" placeholder=""Ingrese su fecha inicial"">
        </div>
        <div class=""col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"">
            <label class=""col-sm-6 col-form-label"">Fecha final:</label>
            <input type=""text"" class=""form-control"" id=""txtFechaFin"" placeholder=""Ingrese se fecha final"">
        </div>

    </div>
    <div class=""row"" style=""padding:1em 2em"">
        <div class=""col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"">
           ");
            WriteLiteral(@" 
        </div>
        <div class=""col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"">
            <button class=""btn btn-secondary float-right"" id=""button"" onclick=""FiltersAdded()"">Aplicar filtros</button>
        </div>

    </div>

    <div id=""divTabla"" style=""padding: 0em 2em"">
        <table id=""tblRepProd"" class=""table table-striped table-bordered dt-responsive"" style=""table-layout:fixed;width:100%"">
            <thead>
                <tr>
                    <th style=""width:12%"">Fecha de emision</th>
                    <th style=""width:12%"">Empresa</th>
                    <th style=""width:12%"">Establecimiento</th>
                    <th style=""width:12%"">Corte</th>
                    <th style=""width:12%"">Monto IVA</th>
                    <th style=""width:12%"">Monto</th>
                    <th style=""width:12%"">NIT</th>
                    <th style=""width:13%"">Receptor</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>");
            WriteLiteral("\r\n    </div>\r\n</section>\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "4c39c2c006c62afa228d4c49a4f54b944844a04b8773", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "4c39c2c006c62afa228d4c49a4f54b944844a04b9812", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n<script>\r\n    $(document).ready(function () {\r\n          var jsonEstablecimiento = \'");
#nullable restore
#line 65 "C:\Users\guill\Documents\umg\Seminario\POS\AdminVisaNet\Views\Reportes\IndexRepVenta.cshtml"
                                Write(Context.Session.GetString("establecimientos"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"';
        jsonEstablecimiento = jsonEstablecimiento.replaceAll('&quot;', '""');
        var lstEstablecimiento = JSON.parse(jsonEstablecimiento);


        lstEstablecimiento.forEach(function (element) {
            var y = document.getElementById(""ddlEstablecimiento"");
            var option = document.createElement(""option"");
            option.text = element.IDEstablecimiento + ""- "" + element.NombreComercial;
            option.value = element.IDEstablecimiento;
            y.add(option);
        });

        $('#tblRepProd').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'excel', 'pdf',
                {
                    extend: 'print',
                    text: 'Imprimir',
                    autoPrint: false
                }
            ]
        });
        MapeoVentas();

    });
    function MapeoVentas() {

        var reque = {
            ""IDCliente"": 0
        };
        $.ajax({
            url: ""/Reportes/LoadRepVenta"",
     ");
            WriteLiteral(@"       type: ""post"",
            dataType: ""json"",
            data: { ""jsonR"": JSON.stringify(reque) },
            success: function (data) {
                console.log(""Funciono cliente"", data);
                console.log(typeof data);
                var cat = JSON.parse(data);

                $('#tblRepProd tbody tr').remove();

                cat.forEach(elementP => {
                    var date = Date.parse(elementP.FechaEmision);
                    console.log(date);
                    console.log(typeof (elementP.FechaCreacion));

                    $(""<tr><td>"" + elementP.FechaEmision.substring(0, 10) + ""</td><td>"" + elementP.IDEmpresa +
                        ""</td><td>"" + elementP.IDEstablecimiento +
                        ""</td><td>"" + elementP.IDCorte + ""</td><td>"" + elementP.MontoIVA +
                        ""</td><td>"" + elementP.Monto + ""</td><td>"" + elementP.NIT + ""</td><td>"" + elementP.Receptor + ""</td></tr> "").appendTo(""#tblRepProd"")

                })
    ");
            WriteLiteral(@"        },
            error: function (error) {
                console.log(`Error ${error}`);

            }
        });
    }
    function FiltersAdded() {
          //filter.IDEstablecimiento =0;
            //filter.FechaFin = DateTime.MinValue;
            //filter.FechaIni = DateTime.MinValue;
            //filter.IDProveedor=0;
            //filter.IDProducto = """";
        $('#tblRepProd tbody tr').remove();
        var idesta = document.getElementById(""ddlEstablecimiento"").value;
        var idcorte = document.getElementById(""txtIDCorte"").value;
        var fechafin = document.getElementById(""txtFechaFin"").value;
        var fechaini = document.getElementById(""txtFechaIni"").value;

        var idc = 0;
 
        console.log(""ddlEstablecimiento "", idesta);
        console.log(""idcorte "", idcorte);
        //if (!idesta) {
        //    ides=parseInt(idesta)
        //}
        if (idcorte) {
            idc = parseInt(idcorte)
        }
        var reque = {
            """);
            WriteLiteral(@"IDEstablecimiento"": idesta, ""FechaFin"": fechafin, ""FechaIni"": fechaini, ""IDProveedor"": 0, ""IDProducto"": """", ""IDCorte"": idc
        };
        $.ajax({
            url: ""/Reportes/LoadRepVentaByFilters"",
            type: ""post"",
            dataType: ""json"",
            data: { ""jsonR"": JSON.stringify(reque) },
            success: function (data) {
                console.log(""Funciono cliente"", data);
                console.log(typeof data);
                var cat = JSON.parse(data);

                //$('#tblRepProd tbody tr').remove();

                cat.forEach(elementP => {
                    var date = Date.parse(elementP.FechaEmision);
                    console.log(date);
                    console.log(typeof (elementP.FechaCreacion));

                    $(""<tr><td>"" + elementP.FechaEmision.substring(0, 10) + ""</td><td>"" + elementP.IDEmpresa +
                        ""</td><td>"" + elementP.IDEstablecimiento +
                        ""</td><td>"" + elementP.IDCorte + ""</td");
            WriteLiteral(@"><td>"" + elementP.MontoIVA +
                        ""</td><td>"" + elementP.Monto + ""</td><td>"" + elementP.NIT + ""</td><td>"" + elementP.Receptor + ""</td></tr> "").appendTo(""#tblRepProd"")

                })
                //$('#tblRepProd').DataTable({
                //    dom: 'Bfrtip',
                //    buttons: [
                //        'excel', 'pdf',
                //        {
                //            extend: 'print',
                //            text: 'Imprimir',
                //            autoPrint: false
                //        }
                //    ]
                //});
            },
            error: function (error) {
                console.log(`Error ${error}`);

            }
        });
    }
</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
