#pragma checksum "C:\Users\guill\Documents\umg\Seminario\POS\AdminVisaNet\Views\Configuracion\IndexConfiguracion.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "69ff306765e8421ac425427fd2605eeb352209b8"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Configuracion_IndexConfiguracion), @"mvc.1.0.view", @"/Views/Configuracion/IndexConfiguracion.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\guill\Documents\umg\Seminario\POS\AdminVisaNet\Views\_ViewImports.cshtml"
using AdminVisaNet;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\guill\Documents\umg\Seminario\POS\AdminVisaNet\Views\_ViewImports.cshtml"
using AdminVisaNet.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\guill\Documents\umg\Seminario\POS\AdminVisaNet\Views\Configuracion\IndexConfiguracion.cshtml"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"69ff306765e8421ac425427fd2605eeb352209b8", @"/Views/Configuracion/IndexConfiguracion.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c645737a9af3543de74dfa52ac432518dc162c6e", @"/Views/_ViewImports.cshtml")]
    public class Views_Configuracion_IndexConfiguracion : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\guill\Documents\umg\Seminario\POS\AdminVisaNet\Views\Configuracion\IndexConfiguracion.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            WriteLiteral(@"<link rel=""stylesheet"" href=""https://use.fontawesome.com/releases/v5.11.2/css/all.css"">
<div class=""container-fluid"">
    <div class=""text-center"">
        <h4 class=""display-9"">Configuración</h4>
    </div>
    <button class=""btn btn-secondary float-right"" id=""btnADD"" style=""display:none;"" onclick=""openModal(1)"">Agregar +</button>
    <div id=""divTabla"" style=""display: none;"">
        
        <table id=""tblConfig"" class=""table table-striped table-bordered dt-responsive"" style=""table-layout:fixed;width:100%;"">
            <thead>
                <tr>
                    <th style=""width:5%"">ID</th>
                    <th style=""width:20%"">Nombre</th>
                    <th style=""width:20%"">Valor</th>
                    <th style=""width:32%"">Descripción</th>
                    <th style=""width:8%"">Accion</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<div class=""modal fade bd-example-modal-lg"" id=""myModal""");
            WriteLiteral(@" role=""dialog"" data-target=""myModal"">
    <div class=""modal-dialog modal-lg"">
        <!-- Modal content-->
        <div class=""modal-content"">
            <div class=""modal-header"">
                <h4 class=""modal-title"" id=""txtMTitulo""></h4>
            </div>
            <div class=""modal-body"">                
                <div id=""divEDITC"" style=""display:none"">
                    <div class=""form-group row"" style=""display: none;"">
                        <div class=""col-sm-12"">
                            <input type=""text"" class=""form-control"" id=""txtRowIDConf"">
                        </div>
                    </div>
                    <div class=""row"">
                        <div class=""col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3"">
                            <label>Nombre:</label>
                        </div>
                        <div class=""col-9 col-sm-9 col-md-9 col-lg-9 col-xl-9"">
                            <input type=""text"" class=""form-control"" id=""txtNombre"" rea");
            WriteLiteral("donly");
            BeginWriteAttribute("placeholder", " placeholder=\"", 2235, "\"", 2249, 0);
            EndWriteAttribute();
            WriteLiteral(@">
                        </div>
                    </div>
                    <div class=""row"">
                        <div class=""col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3"">
                            <label>Valor:</label>
                        </div>
                        <div class=""col-9 col-sm-9 col-md-9 col-lg-9 col-xl-9"">
                            <input type=""text"" class=""form-control"" id=""txtValor""");
            BeginWriteAttribute("placeholder", " placeholder=\"", 2678, "\"", 2692, 0);
            EndWriteAttribute();
            WriteLiteral(@">
                        </div>
                    </div>                 
                    <div class=""row"">
                        <div class=""col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"">
                            <label>Descripcion:</label>
                            <textarea class=""form-control"" id=""txtDescripcion"" rows=""3""></textarea>
                        </div>
                    </div>

                    <div class=""modal-footer"">
                        <div class=""row"">
                            <div class=""col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"">
                                <button type=""button"" class=""btn btn-warning"" onclick=""CheckParams()"">Editar</button>
                            </div>
                            <div class=""col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4"">
                                <button type=""button"" class=""btn btn-danger"" data-dismiss=""modal"">Cerrar</button>
                            </div>

                        </");
            WriteLiteral(@"div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<script src=""https://code.jquery.com/jquery-3.5.1.js"" crossorigin=""anonymous""></script>
<script src=""https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"" integrity=""sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"" crossorigin=""anonymous""></script>
<script src=""https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"" integrity=""sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s"" crossorigin=""anonymous""></script>
<script>
    var jsonResponse;
    var jsonUsuario = '");
#nullable restore
#line 90 "C:\Users\guill\Documents\umg\Seminario\POS\AdminVisaNet\Views\Configuracion\IndexConfiguracion.cshtml"
                  Write(Context.Session.GetString("usuario"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\';\r\n    jsonUsuario = jsonUsuario.replaceAll(\'&quot;\', \'\"\');\r\n\r\n    var jsonRoll = \'");
#nullable restore
#line 93 "C:\Users\guill\Documents\umg\Seminario\POS\AdminVisaNet\Views\Configuracion\IndexConfiguracion.cshtml"
               Write(Context.Session.GetString("rol"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"';
    jsonRoll = jsonRoll.replaceAll('&quot;', '""');

    var objUsuario = JSON.parse(jsonUsuario);
    var objRoll = JSON.parse(jsonRoll);

    var BEditar = false;
    var BEliminar = false;
    $(document).ready(function () {
        var reque = { ""idConfig"": 0, ""description"": """", ""value"": """", ""key"": """", ""idUsuario"": objUsuario.IDUsuario };
        $.ajax({
            url: ""/Configuracion/GET"",
            type: ""post"",
            data: { ""jsonR"": JSON.stringify(reque) },
            dataType: ""json"",
            success: function (data) {
                console.log(typeof data);
                console.log(""Data del response"", data);
                jsonResponse = JSON.parse(data);
                if (jsonResponse.respuesta == 1) {
                    CheckPermiso(objRoll);
                    MapeoConfig(jsonResponse.config);
                } else {
                    alert(""Hubo un error, intente nuevamente"");
                }

            },
            error: function");
            WriteLiteral(@" (error) {
                console.log(`Error ${error}`);

            }
        });
    });

    function MapeoConfig(config) {
        $('#tblConfig tbody tr').remove();
        //$('#tblBeneficio').DataTable().destroy();
        var esconderE =  "" style='display:none;'""
        var esconderEli = "" style='display:none;'""
        if (BEditar) {
            esconderE = """";
        }
        if (BEliminar) {
            esconderEli = """";
        }
        
        var buton = ""<button onclick='openModal(1,this)' id='btnEdit' "" + esconderE + "" class='btn'><i class='fas fa-edit'></i></button><button id='btnDelete' "" + esconderEli + "" onclick='DeleteCategoria(this)' class='btn'><i class='far fa-trash-alt'></i></button>"";
        config.forEach(elementP => {            
            $(""<tr><td id='tdIDConfig' data-name='tdIDConfig'>"" + elementP.idConfig + ""</td><td id='tdKey' data-name='tdKey'>"" + elementP.key + ""</td><td id='tdValor' data-name='tdValor'>"" + elementP.value + ""</td><td id='tdDes");
            WriteLiteral(@"cripcion' data-name='tdDescripcion'>"" + elementP.description + ""</td><td>"" + buton + ""</td>"" + ""</tr>"").appendTo(""#tblConfig"")

        })
        //$('#tblBeneficio').DataTable().draw();
        $('#tblConfig').DataTable();
    }
    function openModal(divID, parametro) {

        console.log(""Entro a openModal"" + divID);
        if (divID == 1) {
            var rowID = parametro.closest(""tr"").rowIndex;
            $(""#txtMTitulo"").html('Editar valor de configuración');
            document.getElementById(""divEDITC"").style.display = ""inline"";      
            var idconfig = $(""#tblConfig tbody tr"")[parseInt(rowID) - 1].cells.namedItem(""tdIDConfig"").innerHTML;
            var valor = $(""#tblConfig tbody tr"")[parseInt(rowID) - 1].cells.namedItem(""tdValor"").innerHTML;
            var descripcion = $(""#tblConfig tbody tr"")[parseInt(rowID) - 1].cells.namedItem(""tdDescripcion"").innerHTML;
            var nombre = $(""#tblConfig tbody tr"")[parseInt(rowID) - 1].cells.namedItem(""tdKey"").innerHTML;
 ");
            WriteLiteral(@"           document.getElementById(""txtRowIDConf"").value = idconfig;
            document.getElementById(""txtDescripcion"").value = descripcion;
            document.getElementById(""txtValor"").value = valor;
            document.getElementById(""txtNombre"").value = nombre;
        } txtRowIDConf
        $('#myModal').modal('show');

    }
    function CheckPermiso(lstPermiso) {
        var permisoCrear = lstPermiso.find(x => x.IDPermiso == 13);
        var permisoBorrar = lstPermiso.find(x => x.IDPermiso == 14);
        var permisoEditar = lstPermiso.find(x => x.IDPermiso == 15);
        var permisoVer = lstPermiso.find(x => x.IDPermiso == 16);

        if (permisoVer.IDEstado == 1) {
            document.getElementById(""divTabla"").style.display = ""inline"";
        }
        //if (permisoCrear.IDEstado == 1) {
        //    document.getElementById(""btnADD"").style.display = ""inline"";
        //}
        if (permisoEditar.IDEstado == 1) {
            BEditar = true;
        }
        //if (");
            WriteLiteral(@"permisoBorrar.IDEstado == 1) {
        //    BEliminar = true;
        //}


    }
    function CheckParams() {
        var idconfig = document.getElementById(""txtRowIDConf"").value;
        var descripcion = document.getElementById(""txtDescripcion"").value;
        var valor =document.getElementById(""txtValor"").value;
        var nombre=document.getElementById(""txtNombre"").value ;

        let error_array = [];
        if (!descripcion) {
            error_array.push('El campo ""Descripcion"" no puede venir vacio');
        }
        if (!valor) {
            error_array.push('El campo ""Valor"" no puede venir vacio');
        }
        if (!nombre) {
            error_array.push('El campo ""Nombre"" no puede venir vacio');
        }
        if (error_array.length == 0) {
            console.log(""Verdadero"");
            EditConfig();
        } else {
            var text = """";
            console.log(""falso"");
            console.log(error_array.length.toString());
            error_arr");
            WriteLiteral(@"ay.forEach(function (elemento) {
                text = text + elemento + ""\n"";
            })
            alert("""" + text);
        }
    }
    function isObject(value) {
        return value && typeof value === 'object' && value.constructor === Object;
    }
    function EditConfig() {
        var idconfig = parseInt(document.getElementById(""txtRowIDConf"").value);
        var descripcion = document.getElementById(""txtDescripcion"").value;
        var valor = document.getElementById(""txtValor"").value;
        var nombre = document.getElementById(""txtNombre"").value;
        var reque = { ""idConfig"": idconfig, ""description"": descripcion, ""value"": valor, ""key"": nombre, ""idUsuario"": objUsuario.IDUsuario };

        $.ajax({
            url: ""/Configuracion/Update"",
            type: ""post"",
            dataType: ""json"",
            data: { ""jsonR"": JSON.stringify(reque) },
            success: function (data) {
                console.log(typeof data);
                console.log(""Data del ");
            WriteLiteral(@"response"", data);
                var ok = isObject(data);
                if (!ok) {
                    jsonResponse = JSON.parse(data);
                    if (jsonResponse.respuesta == 1) {                        
                        MapeoConfig(jsonResponse.config);
                        alert(jsonResponse.descripcion);
                    } else {
                        alert(""Hubo un error, intente nuevamente"");
                    }
                }
                else {
                    alert(""No cuenta con permisos para esta operación"");
                }
            },
            error: function (error) {
                console.log(`Error ${error}`);

            }
        });


    }

</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
