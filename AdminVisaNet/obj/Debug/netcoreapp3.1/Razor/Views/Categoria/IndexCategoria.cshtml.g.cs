#pragma checksum "C:\Users\guill\Documents\umg\Seminario\POS\AdminVisaNet\Views\Categoria\IndexCategoria.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b22f1e31b160f44e48e60479d75709f26a142cec"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Categoria_IndexCategoria), @"mvc.1.0.view", @"/Views/Categoria/IndexCategoria.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\guill\Documents\umg\Seminario\POS\AdminVisaNet\Views\_ViewImports.cshtml"
using AdminVisaNet;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\guill\Documents\umg\Seminario\POS\AdminVisaNet\Views\_ViewImports.cshtml"
using AdminVisaNet.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\guill\Documents\umg\Seminario\POS\AdminVisaNet\Views\Categoria\IndexCategoria.cshtml"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b22f1e31b160f44e48e60479d75709f26a142cec", @"/Views/Categoria/IndexCategoria.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c645737a9af3543de74dfa52ac432518dc162c6e", @"/Views/_ViewImports.cshtml")]
    public class Views_Categoria_IndexCategoria : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/jquery.mCustomScrollbar.concat.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/main.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\guill\Documents\umg\Seminario\POS\AdminVisaNet\Views\Categoria\IndexCategoria.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            WriteLiteral(@"
<section class=""full-width pageContent"">
    <section class=""full-width text-center"" style=""padding: 10px 0;"">
        <h3 class=""text-center tittles"">CATEGORIAS</h3>
    </section>
    <div style=""padding: 0em 2em"">
        <button class=""btn btn-secondary float-right"" id=""btnADD"" onclick=""openModal(1)"">Agregar +</button>
        <div id=""divTabla"">
            <table id=""tblCategoria"" class=""table table-striped table-bordered dt-responsive"" style=""table-layout:fixed;width:100%;"">
                <thead>
                    <tr>
                        <th style=""width:15%"">ID</th>
                        <th style=""width:25%"">Categoria</th>
                        <th style=""width:30%"">Observaciones</th>
                        <th style=""width:15%"">Accion</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

</section>

<div class=""modal fade bd-example-modal-lg"" id=""myModal"" role=""d");
            WriteLiteral(@"ialog"" data-target=""myModal"">
    <div class=""modal-dialog modal-lg"">
        <!-- Modal content-->
        <div class=""modal-content"">
            <div class=""modal-header"">
                <h4 class=""modal-title"" id=""txtMTitulo""></h4>
            </div>
            <div class=""modal-body"">
                <div id=""divADDCat"" style=""display:none"">
                    <div class=""row"">
                        <div class=""col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3"">
                            <p>Categoria:</p>
                        </div>
                        <div class=""col-9 col-sm-9 col-md-9 col-lg-9 col-xl-9"">
                            <input type=""text"" class=""form-control"" id=""txtACategoria""");
            BeginWriteAttribute("placeholder", " placeholder=\"", 1831, "\"", 1845, 0);
            EndWriteAttribute();
            WriteLiteral(@">
                        </div>
                    </div>
                    <div class=""row"">
                        <div class=""col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"">
                            <p>Observaciones:</p>
                            <textarea class=""form-control"" id=""txtAObservaciones"" rows=""3""></textarea>
                        </div>
                    </div>
                    <div class=""modal-footer"">
                        <div class=""row"">
                            <div class=""col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"">
                                <button type=""button"" class=""btn btn-warning"" onclick=""ADDCat()"">Agregar</button>
                            </div>
                            <div class=""col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4"">
                                <button type=""button"" class=""btn btn-danger"" data-dismiss=""modal"">Cerrar</button>
                            </div>

                        </div>
                    ");
            WriteLiteral(@"</div>
                </div>
                <div id=""divEDITCat"" style=""display:none"">
                    <div class=""form-group row"" style=""display: none;"">
                        <div class=""col-sm-12"">
                            <input type=""text"" class=""form-control"" id=""txtIDCAT"">
                        </div>
                    </div>
                    <div class=""row"">
                        <div class=""col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3"">
                            <p>Categoria:</p>
                        </div>
                        <div class=""col-9 col-sm-9 col-md-9 col-lg-9 col-xl-9"">
                            <input type=""text"" class=""form-control"" id=""txtECategoria""");
            BeginWriteAttribute("placeholder", " placeholder=\"", 3594, "\"", 3608, 0);
            EndWriteAttribute();
            WriteLiteral(@">
                        </div>
                    </div>
                    <div class=""row"">
                        <div class=""col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12"">
                            <p>Observaciones:</p>
                            <textarea class=""form-control"" id=""txtEObservaciones"" rows=""3""></textarea>
                        </div>
                    </div>
                    <div class=""modal-footer"">
                        <div class=""row"">
                            <div class=""col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"">
                                <button type=""button"" class=""btn btn-warning"" onclick=""EditCat()"">Editar</button>
                            </div>
                            <div class=""col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4"">
                                <button type=""button"" class=""btn btn-danger"" data-dismiss=""modal"">Cerrar</button>
                            </div>

                        </div>
                    ");
            WriteLiteral("</div>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n\r\n    </div>\r\n</div>\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b22f1e31b160f44e48e60479d75709f26a142cec9544", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b22f1e31b160f44e48e60479d75709f26a142cec10583", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
<script>
    var jsonResponse;
    var BEditar = false;
    var BEliminar = false;
    $(document).ready(function () {
        var reque = {
            ""IDCliente"": 0
        };
        $.ajax({
            url: ""/Categoria/Load"",
            type: ""post"",
            dataType: ""json"",
            data: { ""jsonR"": JSON.stringify(reque)},
            success: function (data) {
                console.log(typeof data);
                jsonResponse = JSON.parse(data);
                if (jsonResponse.Respuesta == 1) {

                } else {
                    alert(jsonResponse.Descripcion);
                }
            },
            error: function (error) {
                console.log(`Error ${error}`);

            }
        });

         var jsonCategoria = '");
#nullable restore
#line 134 "C:\Users\guill\Documents\umg\Seminario\POS\AdminVisaNet\Views\Categoria\IndexCategoria.cshtml"
                         Write(Context.Session.GetString("categorias"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"';
        jsonCategoria = jsonCategoria.replaceAll('&quot;', '""');
        var lstCategoria = JSON.parse(jsonCategoria);
        console.log(lstCategoria);
        MapeoCategoria(lstCategoria);
    });

    function openModal(divID, parametro) {

        console.log(""Entro a openModal"" + divID);
        if (divID == 1) {
            $(""#txtMTitulo"").html('Agregar nueva categoria');
            document.getElementById(""divADDCat"").style.display = ""inline"";
            document.getElementById(""divEDITCat"").style.display = ""none"";
        }
        if (divID == 2) {
            var rowID = parametro.closest(""tr"").rowIndex;
            console.log(""rowID"", rowID);
            $(""#txtMTitulo"").html('Editar categoria');
            document.getElementById(""divEDITCat"").style.display = ""inline"";
            document.getElementById(""divADDCat"").style.display = ""none"";
            var id = $(""#tblCategoria tbody tr"")[parseInt(rowID) - 1].cells.namedItem(""idcat"").innerHTML;
            var catego");
            WriteLiteral(@"ria = $(""#tblCategoria tbody tr"")[parseInt(rowID) - 1].cells.namedItem(""tdCategoria"").innerHTML;
            var observaciones = $(""#tblCategoria tbody tr"")[parseInt(rowID) - 1].cells.namedItem(""tdObservaciones"").innerHTML;
            console.log(""id"", id);

            document.getElementById(""txtIDCAT"").value = id;
            document.getElementById(""txtECategoria"").value = categoria;
            document.getElementById(""txtEObservaciones"").value = observaciones;
        }
        $('#myModal').modal('show');

    }
    function MapeoCategoria(cat) {
        $('#tblCategoria tbody tr').remove();
        //var esconderE =  "" style='display:none;'""
        //var esconderEli = "" style='display:none;'""
        var esconderE = """"
        var esconderEli = """"
        //if (BEditar) {
        //    esconderE = """";
        //}
        //if (BEliminar) {
        //    esconderEli = """";
        //}
        var buton = ""<button onclick='openModal(2,this)' id='btnEdit' "" + esconderE + "" class='");
            WriteLiteral(@"btn'><i class='fas fa-edit'></i></button><button id='btnDelete' "" + esconderEli + "" onclick='confirmDeleteCategoria(this)' class='btn'><i class='far fa-trash-alt'></i></button>"";
        cat.forEach(elementP => {
            var img = ""<img src='"" + elementP.img + ""' class='img-thumbnail' alt='img'>"";
            console.log(img);
            $(""<tr><td id='idcat' data-name='idcat'>"" + elementP.IDCategoria + ""</td><td id='tdCategoria' data-name='tdCategoria'>"" + elementP.Categoria + ""</td><td id='tdObservaciones' data-name='tdObservaciones'>"" + elementP.Observaciones + ""</td><td>"" + buton + ""</td>"" + ""</tr>"").appendTo(""#tblCategoria"")

        })
        //$('#tblBeneficio').DataTable().draw();
        $('#tblCategoria').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'excel', 'pdf',
                {
                    extend: 'print',
                    text: 'Imprimir',
                    autoPrint: false
                }
            ]
        });
    }");
            WriteLiteral(@"
    function ADDCat() {
        console.log(""Entro a ADDCat"");
        var nombre = document.getElementById(""txtACategoria"").value;
        var observacion = document.getElementById(""txtAObservaciones"").value;

        var reque = {
            ""IDCategoria"": 0, ""Categoria"": nombre, ""Observaciones"": observacion
        };
        $.ajax({
            url: ""/Categoria/ADD"",
            type: ""post"",
            dataType: ""json"",
            data: { ""jsonR"": JSON.stringify(reque)},
            success: function (data) {
                console.log(typeof data);
                jsonResponse = JSON.parse(data);
                if (jsonResponse.Respuesta == 1) {
                    MapeoCategoria(jsonResponse.Categoria);
                    alert(jsonResponse.Descripcion);
                    document.getElementById(""txtACategoria"").value = """";
                    document.getElementById(""txtAObservaciones"").value = """";
                } else {
                    alert(jsonResponse.Descripc");
            WriteLiteral(@"ion);
                }
            },
            error: function (error) {
                console.log(`Error ${error}`);

            }
        });
    }
    function EditCat() {
        console.log(""Entro a EditCat"");
        var nombre = document.getElementById(""txtECategoria"").value;
        var id = document.getElementById(""txtIDCAT"").value;
        var observacion = document.getElementById(""txtEObservaciones"").value;

        var reque = {
            ""IDCategoria"": id, ""Categoria"": nombre, ""Observaciones"": observacion
        };
        $.ajax({
            url: ""/Categoria/EDIT"",
            type: ""post"",
            dataType: ""json"",
            data: { ""jsonR"": JSON.stringify(reque) },
            success: function (data) {
                console.log(typeof data);
                jsonResponse = JSON.parse(data);
                if (jsonResponse.Respuesta == 1) {
                    MapeoCategoria(jsonResponse.Categoria);
                    alert(jsonResponse.Descripci");
            WriteLiteral(@"on);
                } else {
                    alert(jsonResponse.Descripcion);
                }
            },
            error: function (error) {
                console.log(`Error ${error}`);

            }
        });
    }

    function confirmDeleteCategoria(parametro) {
        var txt;
        var r = confirm(""¿Seguro desea eliminar la categoría?"");
        if (r == true) {
            deleteCategoria(parametro);
        }
    }

    function deleteCategoria(parametro) {
        var rowID = parametro.closest(""tr"").rowIndex;
        var id = $(""#tblCategoria tbody tr"")[parseInt(rowID) - 1].cells.namedItem(""idcat"").innerHTML;
        console.log(""parametro "", id);
        var reque = {
            ""IDCategoria"": id, ""Nombre"": """", ""NIT"": """", ""Direccion"": """", ""Telefono"": """", ""Email"": """", ""IDEmpresa"": 0
        };
        $.ajax({
            url: ""/Categoria/DELETE"",
            type: ""post"",
            dataType: ""json"",
            data: { ""jsonR"": JSON.stringify(req");
            WriteLiteral(@"ue) },
            success: function (data) {
                console.log(typeof data);
                jsonResponse = JSON.parse(data);
                if (jsonResponse.Respuesta == 1) {
                    alert(jsonResponse.Descripcion);
                    MapeoCategoria(jsonResponse.Categoria);


                    /*MapeoRedes(jsonResponse.redes);*/
                } else {
                    alert(jsonResponse.descripcion);
                }
            },
            error: function (error) {
                console.log(`Error ${error}`);

            }
        });
    }

</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
