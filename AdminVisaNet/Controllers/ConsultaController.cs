﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using POSWebOS.Models;
using POSWebOS.Models.DB;
using POSWebOS.Models.Operations;
using POSWebOS.Models.RequestJson;
using POSWebOS.Models.ResponseJson;

namespace POSWebOS.Controllers
{
    public class ConsultaController : Controller
    {
        OperationDB ops;
        private readonly Context _context;
        public IActionResult IndexConsulta()
        {
            return View();
        }
        public ConsultaController(Context context)
        {

            _context = context;
            ops = new OperationDB(_context);
        }
        public IActionResult LoadConsulta(string jsonR)
        {
            //EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);

            Filters filter = new Filters();
            filter.IDCorte = 0;
            filter.IDEstablecimiento = 0;
            filter.FechaFin = DateTime.MinValue;
            filter.FechaIni = DateTime.MinValue;
            filter.IDProveedor = 0;
            filter.IDProducto = "";
            filter.IDTransaccion =0;
            filter.Serie = "";
            filter.NIT = "";

            List<Consulta> lstConsulta = new List<Consulta>();
            lstConsulta = ops.GetConsulta(filter);
            string response = JsonConvert.SerializeObject(lstConsulta);

            return Json(response);
        }
        public IActionResult LoadConsultaByFilters(string jsonR)
        {
            Filters filter = JsonConvert.DeserializeObject<Filters>(jsonR);


            //Filters filter = new Filters();
            //filter.IDCorte = 0;
            //filter.IDEstablecimiento = 0;
            //filter.FechaFin = DateTime.MinValue;
            //filter.FechaIni = DateTime.MinValue;
            //filter.IDProveedor = 0;
            //filter.IDProducto = "";
            if (filter.FechaFin == null)
            {
                filter.FechaFin = DateTime.MinValue;
            }
            if (filter.FechaIni == null)
            {
                filter.FechaIni = DateTime.MinValue;

            }
            List<Consulta> lstSale = new List<Consulta>();
            lstSale = ops.GetConsulta(filter);
            string ventas = JsonConvert.SerializeObject(lstSale);

            return Json(ventas);
        }

    }
}
