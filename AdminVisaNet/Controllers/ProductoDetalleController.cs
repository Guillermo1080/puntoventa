﻿using AdminVisaNet.Models;
using AdminVisaNet.Models.WSCALL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using POSWebOS.Models;
using POSWebOS.Models.DB;
using POSWebOS.Models.Operations;
using POSWebOS.Models.RequestJson;
using POSWebOS.Models.ResponseJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminVisaNet.Controllers
{
    public class ProductoDetalleController : Controller
    {
        OperationDB ops;
        private readonly Context _context;
        // GET: ProductoDetalleController
        APIcall apicall = new APIcall();

        public ProductoDetalleController(Context context)
        {
            _context = context;
            ops = new OperationDB(_context);
        }
        public ActionResult IndexProductoDetalle() {
            return View();
        }

        public IActionResult EDIT(string jsonR)
        {
            string usuario = HttpContext.Session.GetString("usuario");
            UsuarioModel usua = JsonConvert.DeserializeObject<UsuarioModel>(usuario);
            ProductoRequest req = JsonConvert.DeserializeObject<ProductoRequest>(jsonR);
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);
            req.Producto.IDEmpresa = objEmpresa.IDEmpresa;

            req.MovInvProducto.IDUsuario = usua.IDUsuario;
            
            //req.MovInvProducto.IDEstablecimiento = req.Producto.IDEstablecimiento;

            CorteResponse corteResponse = ops.GetCortesBYIDEmpresa(objEmpresa.IDEmpresa, req.Producto.IDEstablecimiento);

            if (corteResponse.Corte is null)
            {
                GlobalResponse global = new GlobalResponse();
                global.Descripcion = "No hay cortes abiertos para efectuar esta operacion, contacte al jefe de tienda";
                string jsonResponse = JsonConvert.SerializeObject(global);
                return Json(jsonResponse);

            }
            else
            {
                req.MovInvProducto.IDCorte = corteResponse.Corte.IDCorte;
                ops.AddMovInvProducto(req.MovInvProducto);
                ProductoResponse response = ops.EditProducto(req.Producto);
                string jsonResponse = JsonConvert.SerializeObject(response);
                return Json(jsonResponse);

            }


            //if (response.Respuesta == 1)
            //{
            //    string url = "Redes/ADD";
            //    var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
            //    return Json(jsonResponse);

            //}
            //else
            //{
            //    return Json(response);
            //}

        }

        public IActionResult PRODUCTOBYID(string jsonR)
        {
            ProductoModel prd = new ProductoModel();
            ProductoModelE objIdPRD = JsonConvert.DeserializeObject<ProductoModelE>(jsonR);
            prd = ops.GetProductoBYID(objIdPRD.IDProducto);
            ProductoResponseE global = new ProductoResponseE();
            global.Respuesta = 1;
            global.Descripcion = "Traida de producto exitoso";
            global.Producto = prd;
            string jsonResponse = JsonConvert.SerializeObject(global);

            return Json(jsonResponse);
        }

        //public ActionResult IndexProductoDetalle(int id)
        //{

        //    HttpContext.Session.SetString("idprodu", id.ToString());
        //    string usuario = HttpContext.Session.GetString("usuario");
        //    Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
        //    string jsonResponse = apicall.GetPermisosBYIDRoll(usua.IDRoll, usua.IDUsuario);
        //    GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponse);
        //    if (response.Respuesta == 1)
        //    {
        //        string permiso = JsonConvert.SerializeObject(response.DetallePermiso);
        //        HttpContext.Session.SetString("rol", permiso);
        //    }
        //    DetallePermiso permi = new DetallePermiso();
        //    permi = response.DetallePermiso.Find(a => a.IDPermiso == 7);
        //    if (permi.IDEstado == 1)
        //    {
        //        return View();
        //    }
        //    else
        //    {
        //        return RedirectToAction("IndexError", "Home");
        //    }

        //}

        //public IActionResult GetBYID(string jsonR)
        //{         
        //    string url = "Producto/GetByID";
        //    var jsonResponse = "";
        //    jsonResponse= apicall.POSTCALLBODY(jsonR,url);
        //    return Json(jsonResponse);

        //}
        //public IActionResult DeleteBeneBYID(string jsonR)
        //{
        //    string usuario = HttpContext.Session.GetString("usuario");
        //    Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
        //    string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 39);
        //    GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponseG);            
        //    if (response.Respuesta == 1)
        //    {
        //        string url = "Beneficio/DELETE/";
        //        var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
        //        return Json(jsonResponse);

        //    }
        //    else
        //    {
        //        return Json(response);
        //    }       
        //}
        //public IActionResult DeleteIconBYID(string jsonR)
        //{
        //    string usuario = HttpContext.Session.GetString("usuario");
        //    Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
        //    string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 35);
        //    GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponseG);            
        //    if (response.Respuesta == 1)
        //    {
        //        string url = "Imagen/DELETE/";
        //        var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
        //        return Json(jsonResponse);

        //    }
        //    else
        //    {
        //        return Json(response);
        //    } 

        //}
        //public IActionResult DeleteTestiBYID(string jsonR)
        //{
        //    string usuario = HttpContext.Session.GetString("usuario");
        //    Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
        //    string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 43);
        //    GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponseG);         
        //    if (response.Respuesta == 1)
        //    {
        //        string url = "Testimonial/DELETE/";
        //        var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
        //        return Json(jsonResponse);

        //    }
        //    else
        //    {
        //        return Json(response);
        //    }               
        //}
        //public IActionResult ADDBeneBYID(string jsonR)
        //{
        //    string usuario = HttpContext.Session.GetString("usuario");
        //    Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
        //    string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 38);
        //    GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponseG);            
        //    if (response.Respuesta == 1)
        //    {
        //        string url = "Beneficio/ADD";
        //       var  jsonResponse = apicall.POSTCALLBODY(jsonR, url);
        //        return Json(jsonResponse);

        //    }
        //    else
        //    {
        //        return Json(response);
        //    }

        //}
        //public IActionResult ADDIconoBYID(string jsonR)
        //{
        //    string usuario = HttpContext.Session.GetString("usuario");
        //    Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
        //    string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 34);
        //    GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponseG);       
        //    if (response.Respuesta == 1)
        //    {
        //        string url = "Imagen/ADD";
        //        var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
        //        return Json(jsonResponse);

        //    }
        //    else
        //    {
        //        return Json(response);
        //    }

        //}
        //public IActionResult ADDTestiBYID(string jsonR)
        //{
        //    string usuario = HttpContext.Session.GetString("usuario");
        //    Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
        //    string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 42);
        //    GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponseG);            
        //    if (response.Respuesta == 1)
        //    {
        //        string url = "Testimonial/ADD";
        //        var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
        //        return Json(jsonResponse);

        //    }
        //    else
        //    {
        //        return Json(response);
        //    }

        //}

        //public IActionResult EDITBeneBYID(string jsonR)
        //{
        //    string usuario = HttpContext.Session.GetString("usuario");
        //    Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
        //    string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 40);
        //    GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponseG);
        //    if (response.Respuesta == 1)
        //    {
        //        string url = "Beneficio/UPDATE";
        //        var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
        //        return Json(jsonResponse);

        //    }
        //    else
        //    {
        //        return Json(response);
        //    }          

        //}
        //public IActionResult EDITIconBYID(string jsonR)
        //{
        //    string usuario = HttpContext.Session.GetString("usuario");
        //    Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
        //    string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 36);
        //    GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponseG);         
        //    if (response.Respuesta == 1)
        //    {
        //        string url = "Imagen/UPDATE";
        //        var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
        //        return Json(jsonResponse);

        //    }
        //    else
        //    {
        //        return Json(response);
        //    }          
        //}
        //public IActionResult EDITTestiBYID(string jsonR)
        //{
        //    string usuario = HttpContext.Session.GetString("usuario");
        //    Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
        //    string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 44);
        //    GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponseG);
        //    if (response.Respuesta == 1)
        //    {
        //        string url = "Testimonial/UPDATE";
        //        var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
        //        return Json(jsonResponse);

        //    }
        //    else
        //    {
        //        return Json(response);
        //    }      

        //}

        //public IActionResult EditProdu(string jsonR)
        //{
        //    string usuario = HttpContext.Session.GetString("usuario");
        //    Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
        //    string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 7);
        //    GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponseG);            
        //    if (response.Respuesta == 1)
        //    {
        //        string url = "Producto/UPDATE";
        //        var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
        //        return Json(jsonResponse);

        //    }
        //    else
        //    {
        //        return Json(response);
        //    }                  

        //}

    }
}
