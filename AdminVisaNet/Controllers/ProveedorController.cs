﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using POSWebOS.Models;
using POSWebOS.Models.DB;
using POSWebOS.Models.Operations;
using POSWebOS.Models.ResponseJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Controllers
{
    public class ProveedorController : Controller
    {
        OperationDB ops;
        private readonly Context _context;
        public ActionResult IndexProveedor()
        {
            return View();
        }
        public ProveedorController(Context context)
        {
            _context = context;
            ops = new OperationDB(_context);
        }
        public IActionResult Load(string jsonR)
        {
            List<ProveedorModel> lstProveedores = new List<ProveedorModel>();
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);
            lstProveedores = ops.GetProveedoresBYIDEmpresa(objEmpresa.IDEmpresa);
            GlobalResponse global = new GlobalResponse();
            global.Respuesta = 1;
            string jsonResponse = JsonConvert.SerializeObject(global);
            string prov = JsonConvert.SerializeObject(lstProveedores);
            HttpContext.Session.SetString("proveedores", prov);

            return Json(jsonResponse);
        }
        public IActionResult ADD(string jsonR)
        {
            //string usuario = HttpContext.Session.GetString("usuario");
            //Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            //string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 9);
            ProveedorModel prov = JsonConvert.DeserializeObject<ProveedorModel>(jsonR);
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);
            prov.IDEmpresa = objEmpresa.IDEmpresa;
            ProveedorResponse response = ops.AddProveedor(prov);
            string jsonResponse = JsonConvert.SerializeObject(response);

            return Json(jsonResponse);
            //if (response.Respuesta == 1)
            //{
            //    string url = "Redes/ADD";
            //    var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
            //    return Json(jsonResponse);

            //}
            //else
            //{
            //    return Json(response);
            //}

        }
        public IActionResult EDIT(string jsonR)
        {
            //string usuario = HttpContext.Session.GetString("usuario");
            //Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            //string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 9);
            ProveedorModel prov = JsonConvert.DeserializeObject<ProveedorModel>(jsonR);
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);
            prov.IDEmpresa = objEmpresa.IDEmpresa;
            ProveedorResponse response = ops.EditProveedor(prov);
            string jsonResponse = JsonConvert.SerializeObject(response);

            return Json(jsonResponse);
            //if (response.Respuesta == 1)
            //{
            //    string url = "Redes/ADD";
            //    var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
            //    return Json(jsonResponse);

            //}
            //else
            //{
            //    return Json(response);
            //}

        }
        public IActionResult DELETE(string jsonR)
        {
            //string usuario = HttpContext.Session.GetString("usuario");
            //Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            //string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 9);
            ProveedorModel prov = JsonConvert.DeserializeObject<ProveedorModel>(jsonR);
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);
            prov.IDEmpresa = objEmpresa.IDEmpresa;
            ProveedorResponse response = ops.DeleteProveedor(prov);
            string jsonResponse = JsonConvert.SerializeObject(response);

            return Json(jsonResponse);
            //if (response.Respuesta == 1)
            //{
            //    string url = "Redes/ADD";
            //    var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
            //    return Json(jsonResponse);

            //}
            //else
            //{
            //    return Json(response);
            //}

        }
    }
}
