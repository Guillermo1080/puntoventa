﻿using AdminVisaNet.Models;
using POSWebOS.Models.DB;
using AdminVisaNet.Models.WSCALL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using POSWebOS.Models.ResponseJson;
using POSWebOS.Models.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using POSWebOS.Models;

namespace AdminVisaNet.Controllers
{
    public class UsuarioController : Controller
    {
        OperationDB ops;
        APIcall apicall = new APIcall();
        private readonly Context _context;
        // GET: UsuarioController
        public ActionResult IndexUsuario()
        {
            //string usuario = HttpContext.Session.GetString("usuario");
            //Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            //string jsonResponse = apicall.GetPermisosBYIDRoll(usua.IDRoll, usua.IDUsuario);
            //GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponse);
            //if (response.Respuesta == 1)
            //{   
            //    string permiso = JsonConvert.SerializeObject(response.DetallePermiso);
            //    HttpContext.Session.SetString("rol", permiso);

            //}
            return View();
        }
        public UsuarioController(Context context)
        {
            _context = context;
            ops = new OperationDB(_context);
        }
        public IActionResult GET(string jsonR)
        {
            string url = "Usuario";            
            var jsonResponse = "";
            jsonResponse = apicall.POSTCALLBODY(jsonR, url);
            return Json(jsonResponse);
        }
        public IActionResult ADD(string jsonR)
        {   
            string usuario = HttpContext.Session.GetString("usuario");
            Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 29);
            GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponseG);
            var jsonResponse = "";
            if (response.Respuesta == 1)
            {
                string url = "Usuario/ADD";                
                jsonResponse = apicall.POSTCALLBODY(jsonR, url);
                return Json(jsonResponse);

            }
            else
            {
                return Json(response);
            }

        }

        public IActionResult UPDATE(string jsonR)
        {            
            string usuario = HttpContext.Session.GetString("usuario");
            Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 31);
            GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponseG);
            var jsonResponse = "";
            if (response.Respuesta == 1)
            {
                string url = "Usuario/UPDATE";                            
                jsonResponse = apicall.POSTCALLBODY(jsonR, url);
                
                return Json(jsonResponse);

            }
            else
            {
                return Json(response);
            }
        }

        public IActionResult DeleteBYID(string jsonR)
        {
            UsuarioModel usu = JsonConvert.DeserializeObject<UsuarioModel>(jsonR);
            string uempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(uempresa);
            usu.IDEmpresa = objEmpresa.IDEmpresa;
            UsuarioResponse response = ops.DeleteUsuario(usu);
            string jsonResponse = JsonConvert.SerializeObject(response);

            return Json(jsonResponse);
            /*string usuario = HttpContext.Session.GetString("usuario");
            Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 30);
            GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponseG);
            var jsonResponse = "";
            if (response.Respuesta == 1)
            {
                string url = "Usuario/DELETE/" ;          
                jsonResponse = apicall.POSTCALLBODY(jsonR,url);
                return Json(jsonResponse);

            }
            else
            {
                return Json(response);
            }*/
        }
        public IActionResult Reset(string jsonR)
        {            
            string usuario = HttpContext.Session.GetString("usuario");
            Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 33);
            GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponseG);
            var jsonResponse = "";
            if (response.Respuesta == 1)
            {
                string url = "Usuario/RESETBYID";                     
                jsonResponse = apicall.POSTCALLBODY(jsonR, url);
                return Json(jsonResponse);

            }
            else
            {
                return Json(response);
            }


        }
    }
}
