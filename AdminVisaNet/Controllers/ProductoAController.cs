﻿using AdminVisaNet.Models;
using AdminVisaNet.Models.WSCALL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using POSWebOS.Models;
using POSWebOS.Models.DB;
using POSWebOS.Models.Operations;
using POSWebOS.Models.RequestJson;
using POSWebOS.Models.ResponseJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AdminVisaNet.Controllers
{
    public class ProductoAController : Controller
    {
        OperationDB ops;
        private readonly Context _context;
        public ActionResult IndexProductoA()
        {
            return View();
        }
        public ProductoAController(Context context)
        {
            _context = context;
            ops = new OperationDB(_context);
        }
        public IActionResult ADD(string jsonR)
        {
            string usuario = HttpContext.Session.GetString("usuario");
            UsuarioModel usua = JsonConvert.DeserializeObject<UsuarioModel>(usuario);
            //string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 9);
            ProductoRequest obj = JsonConvert.DeserializeObject<ProductoRequest>(jsonR);
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);
            obj.Producto.IDEmpresa = objEmpresa.IDEmpresa;

            obj.MovInvProducto.IDUsuario = usua.IDUsuario;
            //obj.MovInvProducto.IDEmpresa = objEmpresa.IDEmpresa;
            //obj.MovInvProducto.IDEstablecimiento = obj.Producto.IDEstablecimiento;
            

            CorteResponse corteResponse = ops.GetCortesBYIDEmpresa(objEmpresa.IDEmpresa,obj.Producto.IDEstablecimiento);


            if (corteResponse.Corte is null)
            {
                
                GlobalResponse global = new GlobalResponse();
                global.Descripcion = "No hay cortes abiertos para efectuar esta operacion, contacte al jefe de tienda";
                string jsonResponse = JsonConvert.SerializeObject(global);
                return Json(jsonResponse);

            }
            else
            {

                obj.MovInvProducto.IDCorte = corteResponse.Corte.IDCorte;
                if(obj.MovInvProducto.Cantidad>0 && obj.MovInvProducto.IDProveedor > 0)
                {
                    ops.AddMovInvProducto(obj.MovInvProducto);
                }
                 
                ProductoResponse response = ops.AddProducto(obj.Producto);
                string jsonResponse = JsonConvert.SerializeObject(response);
                return Json(jsonResponse);

            }


            
            //if (response.Respuesta == 1)
            //{
            //    string url = "Redes/ADD";
            //    var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
            //    return Json(jsonResponse);

            //}
            //else
            //{
            //    return Json(response);
            //}

        }


    }
}
