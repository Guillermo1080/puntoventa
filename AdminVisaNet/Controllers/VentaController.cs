﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using POSWebOS.Models;
using POSWebOS.Models.DB;
using POSWebOS.Models.Operations;
using POSWebOS.Models.RequestJson;
using POSWebOS.Models.ResponseJson;
using POSWebOS.Models.WSCALL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;

namespace POSWebOS.Controllers
{
    public class VentaController : Controller
    {
        OperationDB ops;
        private readonly Context _context;
        public ActionResult IndexVenta()
        {
            return View();
        }
        public VentaController(Context context)
        {

            _context = context;
            ops = new OperationDB(_context);
        }
        public IActionResult Load(string jsonR)
        {
            List<ClienteModel> lstCliente = new List<ClienteModel>();
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);
            lstCliente = ops.GetClientesBYIDEmpresa(objEmpresa.IDEmpresa);
            GlobalResponse global = new GlobalResponse();
            global.Respuesta = 1;
            string jsonResponse = JsonConvert.SerializeObject(global);
            string clientes = JsonConvert.SerializeObject(lstCliente);
            HttpContext.Session.SetString("clientes", clientes);

            List<ProductoModel> lstProducto = ops.GetProductosBYIDEmpresa(objEmpresa.IDEmpresa);

            string prods = JsonConvert.SerializeObject(lstProducto);
            HttpContext.Session.SetString("productos", prods);

            return Json(jsonResponse);
        }
        public IActionResult GetNITinfo(string jsonR)
        {
            NITRequest objnit = JsonConvert.DeserializeObject<NITRequest>(jsonR);
            WSDigifact ws = new WSDigifact();            
            var jsonResponse = ws.POSTCALLBODY(objnit.NIT);
            return Json(jsonResponse);
        }
        public IActionResult FACTURAR(string jsonR)
        {
            FacturaRequest fact = JsonConvert.DeserializeObject<FacturaRequest>(jsonR);
            fact.Transaccion.APPRequest = jsonR;
            
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);

            string sesta = HttpContext.Session.GetString("establecimientos");
            List<EstablecimientoModel> lstEstablecimiento = JsonConvert.DeserializeObject<List<EstablecimientoModel>>(sesta);
            EstablecimientoModel objEstableciento = lstEstablecimiento.Find(x => x.IDEstablecimiento == fact.Producto[0].IDEstablecimiento);

            string susuario = HttpContext.Session.GetString("usuario");
            UsuarioModel obUsuario = JsonConvert.DeserializeObject<UsuarioModel>(susuario);


            fact.Transaccion.IDEmpresa = objEmpresa.IDEmpresa;
            fact.Transaccion.IDUsuario = obUsuario.IDUsuario;

            CorteResponse corteResponse = ops.GetCortesBYIDEmpresa(objEmpresa.IDEmpresa, fact.Transaccion.IDEstablecimiento);

            if (corteResponse.Corte is null)
            {
                GlobalResponse global = new GlobalResponse();
                global.Descripcion = "No hay cortes abiertos para efectuar esta operacion, contacte al jefe de tienda";
                string jsonResponse = JsonConvert.SerializeObject(global);
                return Json(jsonResponse);

            }
            else {
                fact.Transaccion.IDCorte = corteResponse.Corte.IDCorte;
                fact.Transaccion.NombreReceptor = fact.Transaccion.NombreReceptor.Replace(",,", " ");
                fact.Transaccion.NombreReceptor = fact.Transaccion.NombreReceptor.Replace(",", " ");
                FacturaResponse response = ops.AddTransaccion(fact.Transaccion);
                FacturaTransaccionModel certi = new FacturaTransaccionModel();
                if (response.IDTransaccion > 0)
                {

                    
                    Guid g = Guid.NewGuid();
                    Guid numref = Guid.NewGuid();
                    certi.DocumentGUID = g.ToString().ToUpper();
                    certi.Serie = GetSerie(g.ToString()).ToUpper();
                    certi.NumDoc = GetNumero(g.ToString());
                    certi.IDTransaccion = response.IDTransaccion;
                    certi.InvoiceType = "FACT";
                    certi.XML_Nativo = "XML NATIVO";
                    certi.XML_Signed = "XML SIGNED";
                    certi.NumeroAcceso = "";
                    certi.RefInterna = numref.ToString().ToUpper();
                    certi.Contingencia = false;
                    FacturaTransaccionResponse responseFact = ops.AddFacturaTransaccion(certi);
                    response.Descripcion = response.Descripcion + ", su numero de factura es " + responseFact.FacturaTransaccion.DocumentGUID;

                    

                    foreach (ProductoModel prod in fact.Producto)
                    {
                        ItemTransaccionModel item = new ItemTransaccionModel();
                        item.Cantidad = prod.Cantidad;
                        item.IDProducto = prod.IDProducto;
                        item.IDCategoria = prod.IDCategoria;
                        item.PrecioUnitario = prod.PrecioVenta;
                        item.MontoFacturado = item.Cantidad * prod.PrecioVenta;
                        item.IDTransaccion = response.IDTransaccion;
                        item.Nombre = prod.Nombre;
                        item.Descuento = 0;
                        item.PrecioCosto = prod.PrecioCosto;
                        ops.AddItemTrans(item);
                        MovInvProductoModel obj = new MovInvProductoModel();
                        obj.IDTipoMovProducto = 2;
                        obj.IDCorte=corteResponse.Corte.IDCorte;
                        obj.Cantidad = -(prod.Cantidad);
                        obj.PrecioUnitario = prod.PrecioVenta;
                        obj.IDProducto = prod.IDProducto;
                        obj.Observaciones = "Venta desde la web";
                        obj.IDUsuario = obUsuario.IDUsuario;
                        //obj.IDEstablecimiento = fact.Transaccion.IDEstablecimiento;
                        obj.IDTransaccion = response.IDTransaccion;
                        ops.AddMovInvProducto(obj);
                    }

                }
                EstablecimientoModel fddsfs = new EstablecimientoModel();
                response.DOCGUID = certi.DocumentGUID.ToUpper();
                response.PDF = createPDF(response, fact, objEmpresa, objEstableciento,certi);

                string jsonResponse = JsonConvert.SerializeObject(response);
                return Json(jsonResponse);
            }
            
        }
        private string GetNumero(string DOCGUID)
        {
            string serieDocumento = "";
            string[] DOCGUID_array = DOCGUID.Split('-');
            string NumAuto = DOCGUID_array[1] + DOCGUID_array[2];
            //string NumAuto = "296C48A5";
            serieDocumento = Convert.ToUInt64(NumAuto, 16).ToString();
            return serieDocumento;

        }
        private string GetSerie(string DOCGUID)
        {
            string[] DOCGUID_array = DOCGUID.Split('-');
            return DOCGUID_array[0];
        }


        public string createPDF(FacturaResponse factResponse, FacturaRequest factRequest, EmpresaModel emp, EstablecimientoModel esta, FacturaTransaccionModel certi)
        {
            string path = @"C:\Users\Diego\Desktop\POS\AdminVisaNet\PDF\" + factResponse.IDTransaccion + ".pdf";
            Document doc = new Document();
            PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create)); // asignamos el nombre de archivo hola.pdf
                                                                               // Importante Abrir el documento
            doc.Open();

            Paragraph p1 = new Paragraph();
            p1.Alignment = 1;  //this is aligns to center
            p1.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLACK);
            p1.Add(emp.NIT.ToUpper());
            doc.Add(p1);

            Paragraph p2 = new Paragraph();
            p2.Alignment = 1;  //this is aligns to center
            p2.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLACK);
            p2.Add(emp.RazonSocial);
            doc.Add(p2);

            //doc.Add(new Paragraph(" "));

            Paragraph p3 = new Paragraph();
            p3.Alignment = 1;  //this is aligns to center
            p3.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLACK);
            p3.Add(emp.Direccion);
            doc.Add(p3);


            Paragraph p4 = new Paragraph();
            p4.Alignment = 1;  //this is aligns to center
            p4.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLACK);
            p4.Add(esta.NombreComercial);
            doc.Add(p4);

            Paragraph p5 = new Paragraph();
            p5.Alignment = 1;  //this is aligns to center
            p5.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLACK);
            p5.Add(esta.Direccion);
            doc.Add(p5);

            doc.Add(new Paragraph(" "));

            Paragraph p6 = new Paragraph();
            p6.Alignment = 1;
            p6.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLUE);
            p6.Add("DOCUMENTO TRIBUTARIO ELECTRONICO");
            doc.Add(p6);


            Paragraph p7 = new Paragraph();
            p7.Alignment = 1;
            p7.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLACK);
            p7.Add("N0. DE AUTORIZACIÓN: " + certi.DocumentGUID.ToUpper());
            doc.Add(p7);

            Paragraph p8 = new Paragraph();
            p8.Alignment = 1;
            p8.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLACK);
            p8.Add("SERIE: " + certi.Serie.ToUpper());
            doc.Add(p8);

            Paragraph p9 = new Paragraph();
            p9.Alignment = 1;
            p9.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLACK);
            p9.Add("NUMERO: " + certi.NumDoc);
            doc.Add(p9);

            Paragraph p10 = new Paragraph();
            p10.Alignment = 1;
            p10.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLACK);
            p10.Add("FECHA DE CERTIFICACION: " + certi.FechaCertificacion);
            doc.Add(p10);

            doc.Add(new Paragraph(" "));

            Paragraph p11 = new Paragraph();
            p11.Alignment = 1;
            p11.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLUE);
            p11.Add("DATOS DE COMPRADOR");
            doc.Add(p11);


            Paragraph p12 = new Paragraph();
            p12.Alignment = 1;
            p12.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLACK);
            p12.Add("NIT: " + factRequest.Transaccion.NitReceptor);
            doc.Add(p12);


            Paragraph p13 = new Paragraph();
            p13.Alignment = 1;
            p13.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLACK);
            p13.Add(factRequest.Transaccion.NombreReceptor);
            doc.Add(p13);


            doc.Add(new Paragraph(" "));

            Paragraph p14 = new Paragraph();
            p14.Alignment = 1;
            p14.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLUE);
            p14.Add("DESCRIPCION DE LA FACTURA");
            doc.Add(p14);



            // Empezamos a crear la tabla, definimos una tabla de 6 columnas
            PdfPTable table = new PdfPTable(6);
            table.SpacingBefore = 10;
            // Esta es la primera fila
            table.AddCell("No");
            table.AddCell("Cantidad");
            table.AddCell("Producto");
            table.AddCell("Precio");
            table.AddCell("Descuento");
            table.AddCell("Subtotal");
            int cont = 1;
            decimal total = 0;
            double iva = 0;
            decimal subt = 0;
            foreach (ProductoModel prod in factRequest.Producto)
            {
                table.AddCell(cont.ToString());
                table.AddCell(prod.Cantidad.ToString());
                table.AddCell(prod.Nombre);
                table.AddCell("Q." + prod.PrecioVenta);
                table.AddCell("0");
                decimal subtotal = prod.PrecioVenta * prod.Cantidad;
                total += subtotal;
                table.AddCell("Q" + subtotal.ToString());
                cont++;
            }
            iva = (Convert.ToDouble(total) * 0.12);
            //for(int x = 1; x < cont; x++)
            //{
            //    doc.Add(new Paragraph(" "));
            //    doc.Add(new Paragraph(" "));

            //}            

            // Agregamos la tabla al documento
            doc.Add(table);

            Paragraph t1 = new Paragraph();
            t1.Alignment = Element.ALIGN_RIGHT;

            t1.Alignment = 2;
            t1.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLACK);
            t1.Add("SUBTOTAL: Q" + total.ToString("0.00"));
            doc.Add(t1);

            Paragraph t4 = new Paragraph();
            t4.Alignment = Element.ALIGN_RIGHT;
            t4.Alignment = 2;
            t4.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLACK);
            t4.Add("SUBTOTAL: Q0.00");
            doc.Add(t4);

            Paragraph t2 = new Paragraph();
            t2.Alignment = Element.ALIGN_RIGHT;
            t2.Alignment = 2;
            t2.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLACK);
            t2.Add("IVA: Q" + iva.ToString("0.00"));
            doc.Add(t2);

            Paragraph t3 = new Paragraph();
            t3.Alignment = Element.ALIGN_RIGHT;
            t3.Alignment = 2;
            t3.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLACK);
            t3.Add("TOTAL: Q" + total.ToString("0.00"));
            doc.Add(t3);


            Paragraph p15 = new Paragraph();
            p15.Alignment = 1;
            p15.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLACK);
            p15.Add("ID DE LA TRANSACCION:" + factResponse.IDTransaccion);
            doc.Add(p15);

            Paragraph p16 = new Paragraph();
            p16.Alignment = 1;
            p16.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLACK);
            p16.Add("SUJETO A PAGO TRIMESTRALES");
            doc.Add(p16);

            doc.Add(new Paragraph(" "));

            Paragraph p17 = new Paragraph();
            p17.Alignment = 1;
            p17.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLUE);
            p17.Add("DATOS DEL CERTIFICADOR");
            doc.Add(p17);

            Paragraph p19 = new Paragraph();
            p19.Alignment = 1;
            p19.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLACK);
            p19.Add("NIT: 5345345-K");
            doc.Add(p19);

            Paragraph p18 = new Paragraph();
            p18.Alignment = 1;
            p18.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.BLACK);
            p18.Add("CERTIFICACIONES TRIBUTARIAS GUATEMALA S.A");
            doc.Add(p18);

            Paragraph p20 = new Paragraph();
            p20.Alignment = 1;
            p20.Font = FontFactory.GetFont(FontFactory.TIMES, 10f, BaseColor.MAGENTA);
            p20.Add("GRACIAS POR TU PREFERENCIA Y QUE DISFRUTES TU COMPRA");
            doc.Add(p20);



            // Ceramos el documento
            doc.Close();
            //Una vez cerrado el documento, se guarda en la ubicación que le asignamos, en la linea donde asig


            byte[] bytes = System.IO.File.ReadAllBytes(path);
            if (!string.IsNullOrEmpty(factRequest.Email)){
                SendEmail(factRequest.Email,bytes,factRequest.Transaccion.NombreReceptor,certi.DocumentGUID);
            }
            

            return Convert.ToBase64String(bytes, 0, bytes.Length); ;

        }

        private int SendEmail(string email,byte[] array,string razon,string docguid)
        {
            int respuesta = 0;
            try
            {
                string[] MAILTO_ARRAY = email.Split(',');
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("diegorgh92@gmail.com");

                if (MAILTO_ARRAY.Length > 0)
                {
                    foreach (string themail in MAILTO_ARRAY)
                    {
                        if (!string.IsNullOrEmpty(themail))
                        {
                            mail.To.Add(new MailAddress(themail));
                        }
                    }
                }
                //mail.Bcc.Add = "";
                mail.Subject = "DOCUMENTO TRIBUTARIO ELECTRONICO " ;
                mail.IsBodyHtml = true;
                var stream = new MemoryStream(array);
                mail.Attachments.Add(new Attachment(stream, "FACT_"+ docguid.ToUpper() + ".pdf"));
                string htmlbody = GetBodyTML(razon, docguid);
                string imgPath = AppDomain.CurrentDomain.BaseDirectory;
                LinkedResource LinkedImage = new LinkedResource(@"C:\Users\Diego\Pictures\logo.png");
                LinkedImage.ContentId = "ARTUROLOGO";
                //Added the patch for Thunderbird as suggested by Jorge
                LinkedImage.ContentType = new ContentType(MediaTypeNames.Image.Jpeg);

                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(htmlbody,
                  null, "text/html");
                htmlView.LinkedResources.Add(LinkedImage);
                mail.AlternateViews.Add(htmlView);

                //int Numero_Puerto = Int32.Parse(Port);
                //SmtpServer.Port = Numero_Puerto;
                int Numero_Puerto = 587;
                SmtpServer.Port = Numero_Puerto;
                SmtpServer.Credentials = new System.Net.NetworkCredential("diegorgh92@gmail.com", "FannySassyZiva1!");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                respuesta = 1;

            }
            catch(Exception ex)
            {
                
            }

            return respuesta;

        }
        public string GetBodyTML(string razonSocialComprador, string DOCGUID)
        {
            string body;
            body = @"<html lang='en'>
                        <head>

                        <meta charset='utf-8'>
                        <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>

   
                        <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'>

                        <title>Documento electronico tributario</title>
                      </head>
                      <body >
                        <div class='Container' id='MainConteiner' 
                          style='background-color:#376FB9;'>
                            <div class='container-fluid'>
                                <div class='row' style='padding-top: 1em;'> 
                                    <div class='col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3'>     
                                        <img src=cid:ARTUROLOGO height='75px' class='rounded float-left' alt='LOGO ICON'>                                   
                                    </div>
                                    <div class='col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6'> 
                           
                                    </div>
                                     <div class='col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3'>                                                      
                                    </div>
                                </div>             
                          </div> 
                        </div>
                        </div> 
                        <div class='Container' id='MainConteiner2' 
                          style='background-color:#358AFA;'>
                          <div class='row' style='padding-top: 1em;'> 
                            <div class='col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12'>     
                               <h4><p style='color: cornsilk;'>Estimado Cliente: " + razonSocialComprador + @"</p></h4>                    
                            </div>               
                        </div>
                        <div class='row' style='padding-top: 1em;'> 
                            <div class='col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12'>     
                               <p style='color: cornsilk;'>
                                Se adjunta PDF de su documento electronico tributario " + DOCGUID + @" de servicios adquiridos con su proveedor POSWEB
                                </p>                   
                            </div>               
                        </div>  
                        <div class='row' style='padding-top: 1em;'> 
                            <div class='col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12'>     
                               <h5><p style='color: cornsilk;'>Gracias por su preferencia.</p></h5>                    
                            </div>               
                        </div>
                        </div> 
    
                        <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
                        <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
                        <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>
                      </body>
                    </html>";

            return body;
        }






    }
}
