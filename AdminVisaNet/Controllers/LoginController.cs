﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using AdminVisaNet.Models;
using AdminVisaNet.Models.WSCALL;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using POSWebOS.Models;
using POSWebOS.Models.DB;
using POSWebOS.Models.Operations;
using POSWebOS.Models.Reports;
using POSWebOS.Models.RequestJson;
using POSWebOS.Models.ResponseJson;
using POSWebOS.Models.WSCALL;

namespace AdminVisaNet.Controllers
{
      
    [AllowAnonymous]
    public class LoginController : Controller
    {
        OperationDB ops;
        private readonly Context _context;
        public LoginController(Context context) {            
            EmpresaModel objEmpresa=new EmpresaModel();
            _context = context;
            ops = new OperationDB(_context);

        }
    
        readonly APIcall apicall = new APIcall();
  
        // GET: LoginController
        [AllowAnonymous]
        [HttpGet]
        public ActionResult IndexLogin()
        {
            ViewData["page"] = "Contact";
            return View();
        }        

        public IActionResult Reset(string jsonR)
        {             

            string url = "Usuario/RESET";
            var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
            return Json(jsonResponse);

        }
        
        public IActionResult LoginAUTH(string jsonR)
        {
            //Filters filter = new Filters();
            //filter.IDCorte = 2;
            //filter.IDEstablecimiento =0;
            //filter.FechaFin = DateTime.MinValue;
            //filter.FechaIni = DateTime.MinValue;
            //filter.IDProveedor=0;
            //filter.IDProducto = "";

            //List<ReportSale> lstSale = new List<ReportSale>();
            //lstSale = ops.GetReportSale(filter);

            //List<ReportProducto> lstProd = new List<ReportProducto>();
            //lstProd= ops.GetReportProducto(filter);

            //List<ReportProveedor> lstProve = new List<ReportProveedor>();
            //lstProve = ops.GetReportProveedor(filter);

            //List<ReportInventario> lstInv = new List<ReportInventario>();
            //lstInv = ops.GetReportInventario(filter);
            

            //string jsonReportSale = JsonConvert.SerializeObject(lstSale);
            //string jsonReportProducto = JsonConvert.SerializeObject(lstProd);
            //string jsonReportProveedor = JsonConvert.SerializeObject(lstProve);
            //string jsonReportInventario = JsonConvert.SerializeObject(lstInv);

            LoginRequest req = JsonConvert.DeserializeObject<LoginRequest>(jsonR);
            GlobalResponse global = new GlobalResponse();

            EmpresaModel emp = new EmpresaModel();            
            emp = ops.GetEmpresaBYIDEmpresa(req.NIT);

            if (emp != null)
            {                        
                UsuarioModel usua = ops.GetLogin(req.User, req.Pass);
                if (usua!=null)
                {
                    List<UsuarioModel> lstUsuarios = ops.GetUsuariosBYIDEmpresa(emp.IDEmpresa);
                    List<EstablecimientoModel> lstEstablecimiento = ops.GetEstablecimientosBYIDEmpresa(emp.IDEmpresa);
                    List<ProveedorModel> lstProveedores = ops.GetProveedoresBYIDEmpresa(emp.IDEmpresa);
                    List<CategoriaModel> lstCategoria = ops.GetCategoriaBYIDEmpresa(emp.IDEmpresa);
                    List<ClienteModel> lstCliente = ops.GetClientesBYIDEmpresa(emp.IDEmpresa);
                    List<ProductoModel> lstProducto = ops.GetProductosBYIDEmpresa(emp.IDEmpresa);
                    List<RolModel> lstRoles = ops.GetRolesYIDEmpresa(emp.IDEmpresa);
                    //ops.GetStockBYID();

                    string establecimiento = JsonConvert.SerializeObject(lstEstablecimiento);
                    string empresa = JsonConvert.SerializeObject(emp);
                    string usuario = JsonConvert.SerializeObject(usua);
                    string roles = JsonConvert.SerializeObject(lstRoles);
                    string proveedores = JsonConvert.SerializeObject(lstProveedores);
                    string categoria = JsonConvert.SerializeObject(lstCategoria);
                    string usuarios = JsonConvert.SerializeObject(lstUsuarios);
                    string clientes = JsonConvert.SerializeObject(lstCliente);
                    string productos = JsonConvert.SerializeObject(lstProducto);
                    HttpContext.Session.SetString("appKey", "InternaKeyVisa");
                    HttpContext.Session.SetString("usuario", usuario);
                    HttpContext.Session.SetString("roles", roles);
                    HttpContext.Session.SetString("categorias", categoria);
                    HttpContext.Session.SetString("proveedores", proveedores);
                    HttpContext.Session.SetString("usuarios", usuarios);
                    HttpContext.Session.SetString("clientes", clientes);
                    HttpContext.Session.SetString("productos", productos);
                    HttpContext.Session.SetString("empresa", empresa);
                    HttpContext.Session.SetString("establecimientos", establecimiento);
                    global.Respuesta = 1;

                                       

                    //WSDigifact ws = new WSDigifact();
                    //var hola=ws.POSTCALLBODY("75272059");
                    //Console.WriteLine("dasa");
                    //WSResponse reqss = JsonConvert.DeserializeObject<WSResponse>(hola);


                }
            }
            else
            {
                global.Respuesta = 0;
            }            

            var jsonResponse = JsonConvert.SerializeObject(global);
                        
            //    if (response.Respuesta == 1) {
            //        string usua = JsonConvert.SerializeObject(response.usuario);
            //        string permiso = JsonConvert.SerializeObject(response.DetallePermiso);
            //        string config = JsonConvert.SerializeObject(response.Config);
            //        HttpContext.Session.SetString("usua", jsonResponse);
            //        HttpContext.Session.SetString("usuario", usua);
            //        HttpContext.Session.SetString("rol", permiso);                    
            //        MemoryCache.Default["config"] = config;
            
            

            return Json(jsonResponse);
        }

        //public IActionResult LoginOUT()
        //{
        //    GlobalResponse response = new GlobalResponse();
        //    try
        //    {
        //        HttpContext.Session.SetString("usua", "");
        //        HttpContext.Session.SetString("appKey", "");            
        //        response.Respuesta = 1;
        //        response.Descripcion = "Logout exitoso";
        //        return RedirectToAction("IndexLogin", "Login");
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Respuesta = 0;
        //        response.Descripcion = "Error";
        //        Console.WriteLine("ERROR " + ex.ToString());
        //    }
        //    return Json(response);
        //}
    }
}
