﻿using AdminVisaNet.Models;
using AdminVisaNet.Models.WSCALL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using POSWebOS.Models;
using POSWebOS.Models.DB;
using POSWebOS.Models.Operations;
using POSWebOS.Models.RequestJson;
using POSWebOS.Models.ResponseJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace AdminVisaNet.Controllers
{
    public class ProductoController : Controller
    {
        OperationDB ops;
        private readonly Context _context;
        public ActionResult IndexProducto()
        {
            return View();
        }
        public ProductoController(Context context)
        {
            _context = context;
            ops = new OperationDB(_context);
        }
        public IActionResult Load(string jsonR)
        {
            List<ProductoModel> lstProveedores = new List<ProductoModel>();
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);
            lstProveedores = ops.GetProductosBYIDEmpresa(objEmpresa.IDEmpresa);
            GlobalResponse global = new GlobalResponse();
            global.Respuesta = 1;
            string jsonResponse = JsonConvert.SerializeObject(global);
            string prov = JsonConvert.SerializeObject(lstProveedores);
            HttpContext.Session.SetString("productos", prov);

            return Json(jsonResponse);
        }
        public IActionResult DELETE(string jsonR)
        {
            //string usuario = HttpContext.Session.GetString("usuario");
            //Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            //string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 9);
            ProductoModel prod = JsonConvert.DeserializeObject<ProductoModel>(jsonR);
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);
            prod.IDEmpresa = objEmpresa.IDEmpresa;
            ProductoResponse response = ops.DeleteProducto(prod);
            string jsonResponse = JsonConvert.SerializeObject(response);

            return Json(jsonResponse);
            //if (response.Respuesta == 1)
            //{
            //    string url = "Redes/ADD";
            //    var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
            //    return Json(jsonResponse);

            //}
            //else
            //{
            //    return Json(response);
            //}

        }
        public IActionResult UploadInventario(string jsonR)
        {
            //string usuario = HttpContext.Session.GetString("usuario");
            //Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            //string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 9);

            GlobalResponse global = new GlobalResponse();
            
            

            try
            {
                InventarioRequest req = JsonConvert.DeserializeObject<InventarioRequest>(jsonR);
                string sempresa = HttpContext.Session.GetString("empresa");
                EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);

                
                foreach (Inventario inv in req.Inventario)
                {
                    CorteResponse corte=ops.GetCortesBYIDEmpresa(objEmpresa.IDEmpresa, inv.IDEstablecimiento);
                    if (corte != null)
                    {
                        ProductoModel prod = ops.GetProductoBYID(inv.IDProducto);
                        if (prod != null)
                        {
                            MovInvProductoModel mov = new MovInvProductoModel();
                            mov.IDProducto = prod.IDProducto;
                            mov.PrecioUnitario = prod.PrecioVenta;
                            mov.Cantidad = inv.Cantidad;
                            mov.Documento = inv.Documento;
                            mov.IDTipoMovProducto = 1;
                            mov.IDCorte = corte.Corte.IDCorte;
                            mov.Observaciones = inv.Observaciones;
                            mov.IDProveedor = inv.IDProveedor;
                            ops.AddMovInvProducto(mov);                            
                        }
                        else
                        {
                            global.Respuesta = 0;
                            
                        }
                    }
                    else
                    {
                        Console.WriteLine("corte no abierto");
                    }
                    

                }                
                Load("");
                global.Respuesta = 1;
                global.Descripcion = "Inventario subido exitosamente";
                string jsonResponse = JsonConvert.SerializeObject(global);
                return Json(jsonResponse);


            }
            catch            
            {
                global.Respuesta = 0;
                global.Descripcion = "Revise los datos del archivo excel";
                string jsonResponse = JsonConvert.SerializeObject(global);
                return Json(jsonResponse);

            }
            
            

        }
        public IActionResult UploadProducto(string jsonR)
        {
            //string usuario = HttpContext.Session.GetString("usuario");
            //Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            //string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 9);

            GlobalResponse global = new GlobalResponse();



            try
            {
                ProductoRequest req = JsonConvert.DeserializeObject<ProductoRequest>(jsonR);
                string sempresa = HttpContext.Session.GetString("empresa");
                EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);


                foreach (ProductoModel inv in req.Productos)
                {
                    CorteResponse corte = ops.GetCortesBYIDEmpresa(objEmpresa.IDEmpresa, inv.IDEstablecimiento);
                    if (corte != null)
                    {
                        ProductoModel prod = ops.GetProductoBYID(inv.IDProducto);
                        if (prod == null)
                        {
                            inv.IDEmpresa = objEmpresa.IDEmpresa;
                            
                            ops.AddProducto(inv);
                        }
                        else
                        {
                            global.Respuesta = 0;

                        }
                    }
                    else
                    {
                        Console.WriteLine("corte no abierto");
                    }


                }
                Load("");
                global.Respuesta = 1;
                global.Descripcion = "Inventario subido exitosamente";
                string jsonResponse = JsonConvert.SerializeObject(global);
                return Json(jsonResponse);


            }
            catch(Exception ex)
            {
                global.Respuesta = 0;
                global.Descripcion = "Revise los datos del archivo excel";
                string jsonResponse = JsonConvert.SerializeObject(global);
                return Json(jsonResponse);

            }



        }


    }
}
