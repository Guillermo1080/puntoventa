﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AdminVisaNet.Models;
using AdminVisaNet.Models.WSCALL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using POSWebOS.Models;
using POSWebOS.Models.DB;
using POSWebOS.Models.Operations;
using POSWebOS.Models.ResponseJson;

namespace AdminVisaNet.Controllers
{

    public class CategoriaController : Controller
    {
        OperationDB ops;
        private readonly Context _context;
        public ActionResult IndexCategoria()
        {
            return View();
        }
        public CategoriaController(Context context)
        {
            _context = context;
            ops = new OperationDB(_context);
        }
        public IActionResult Load(string jsonR)
        {
            List<CategoriaModel> lstCategoria = new List<CategoriaModel>();
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);
            lstCategoria = ops.GetCategoriaBYIDEmpresa(objEmpresa.IDEmpresa);
            GlobalResponse global = new GlobalResponse();
            global.Respuesta = 1;
            string jsonResponse = JsonConvert.SerializeObject(global);
            string prov = JsonConvert.SerializeObject(lstCategoria);
            HttpContext.Session.SetString("categorias", prov);

            return Json(jsonResponse);
        }
        public IActionResult ADD(string jsonR)
        {
            //string usuario = HttpContext.Session.GetString("usuario");
            //Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            //string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 9);
            CategoriaModel cat = JsonConvert.DeserializeObject<CategoriaModel>(jsonR);
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);
            cat.IDEmpresa = objEmpresa.IDEmpresa;
            CategoriaResponse response = ops.AddCategoria(cat);
            string jsonResponse = JsonConvert.SerializeObject(response);

            return Json(jsonResponse);
            //if (response.Respuesta == 1)
            //{
            //    string url = "Redes/ADD";
            //    var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
            //    return Json(jsonResponse);

            //}
            //else
            //{
            //    return Json(response);
            //}

        }
        public IActionResult EDIT(string jsonR)
        {
            //string usuario = HttpContext.Session.GetString("usuario");
            //Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            //string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 9);
            CategoriaModel cat = JsonConvert.DeserializeObject<CategoriaModel>(jsonR);
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);
            cat.IDEmpresa = objEmpresa.IDEmpresa;
            CategoriaResponse response = ops.EditCategoria(cat);
            string jsonResponse = JsonConvert.SerializeObject(response);

            return Json(jsonResponse);
            //if (response.Respuesta == 1)
            //{
            //    string url = "Redes/ADD";
            //    var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
            //    return Json(jsonResponse);

            //}
            //else
            //{
            //    return Json(response);
            //}

        }

        public IActionResult DELETE(string jsonR)
        {
            //string usuario = HttpContext.Session.GetString("usuario");
            //Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            //string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 9);
            CategoriaModel categoria = JsonConvert.DeserializeObject<CategoriaModel>(jsonR);
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);
            categoria.IDEmpresa = objEmpresa.IDEmpresa;
            CategoriaResponse response = ops.DeleteCategoria(categoria);
            string jsonResponse = JsonConvert.SerializeObject(response);

            return Json(jsonResponse);
            //if (response.Respuesta == 1)
            //{
            //    string url = "Redes/ADD";
            //    var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
            //    return Json(jsonResponse);

            //}
            //else
            //{
            //    return Json(response);
            //}

        }

    }
}
