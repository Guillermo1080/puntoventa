﻿using AdminVisaNet.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using POSWebOS.Models;
using POSWebOS.Models.DB;
using POSWebOS.Models.Operations;
using POSWebOS.Models.ResponseJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Controllers
{
    public class Cliente : Controller
    {
        OperationDB ops;
        private readonly Context _context;
        public ActionResult IndexCliente()
        {
            return View();
        }
        public Cliente(Context context)
        {
            
            _context = context;
            ops = new OperationDB(_context);            
        }
        public IActionResult Load(string jsonR)
        {
            List<ClienteModel> lstCliente = new List<ClienteModel>();            
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);            
            lstCliente = ops.GetClientesBYIDEmpresa(objEmpresa.IDEmpresa);
            GlobalResponse global = new GlobalResponse();
            global.Respuesta = 1;
            string jsonResponse = JsonConvert.SerializeObject(global);
            string clientes = JsonConvert.SerializeObject(lstCliente);
            HttpContext.Session.SetString("clientes", clientes);

            return Json(jsonResponse);
        }
        public IActionResult ADD(string jsonR)
        {
            //string usuario = HttpContext.Session.GetString("usuario");
            //Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            //string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 9);
            ClienteModel cliente = JsonConvert.DeserializeObject<ClienteModel>(jsonR);
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);
            cliente.IDEmpresa = objEmpresa.IDEmpresa;
            ClienteResponse response = ops.AddCliente(cliente);
            string jsonResponse = JsonConvert.SerializeObject(response);
            
            return Json(jsonResponse);
            //if (response.Respuesta == 1)
            //{
            //    string url = "Redes/ADD";
            //    var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
            //    return Json(jsonResponse);

            //}
            //else
            //{
            //    return Json(response);
            //}

        }
        public IActionResult EDIT(string jsonR)
        {
            //string usuario = HttpContext.Session.GetString("usuario");
            //Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            //string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 9);
            ClienteModel cliente = JsonConvert.DeserializeObject<ClienteModel>(jsonR);
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);
            cliente.IDEmpresa = objEmpresa.IDEmpresa;
            ClienteResponse response = ops.EditCliente(cliente);
            string jsonResponse = JsonConvert.SerializeObject(response);

            return Json(jsonResponse);
            //if (response.Respuesta == 1)
            //{
            //    string url = "Redes/ADD";
            //    var jsonResponse = apicall.POSTCALLBODY(jsonR, url);
            //    return Json(jsonResponse);

            //}
            //else
            //{
            //    return Json(response);
            //}

        }

        public IActionResult DELETE(string jsonR)
        {
            ClienteModel cliente = JsonConvert.DeserializeObject<ClienteModel>(jsonR);
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel objEmpresa = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);
            cliente.IDEmpresa = objEmpresa.IDEmpresa;
            ClienteResponse response = ops.DeleteCliente(cliente);
            string jsonResponse = JsonConvert.SerializeObject(response);
            return Json(jsonResponse);
        }
    }
}
