﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AdminVisaNet.Models;
using AdminVisaNet.Models.WSCALL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using POSWebOS.Models.ResponseJson;

namespace AdminVisaNet.Controllers
{
    public class RolDetalleController : Controller
    {
        //private readonly ILogger<RolDetalleController> _logger;
        //private readonly IHttpClientFactory _clientFactory;
        APIcall apicall = new APIcall();
        public RolDetalleController(ILogger<RolDetalleController> logger)
        {
            //_logger = logger;
        }
        // GET: ProductoController
        //public ActionResult IndexRolDetalle(int id)
        //{
        //    HttpContext.Session.SetString("id", id.ToString());
        //    string usuario = HttpContext.Session.GetString("usuario");
        //    Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
        //    string jsonResponse = apicall.GetPermisosBYIDRoll(usua.IDRoll, usua.IDUsuario);
        //    GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponse);
        //    if (response.Respuesta == 1)
        //    {
        //        string permiso = JsonConvert.SerializeObject(response.DetallePermiso);
        //        HttpContext.Session.SetString("rol", permiso);
        //    }
        //    DetallePermiso permi = new DetallePermiso();
        //    permi = response.DetallePermiso.Find(a => a.IDPermiso == 27);
        //    if (permi.IDEstado == 1)
        //    {
        //        return View();

        //    }
        //    else
        //    {
        //        return RedirectToAction("IndexError", "Home");
        //    }

        //}
        public IActionResult ADD(string jsonR)
        {           

            string usuario = HttpContext.Session.GetString("usuario");
            Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 25);
            GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponseG);
            var jsonResponse = "";
            if (response.Respuesta == 1)
            {
                string url = "Rol/ADD";                             
                jsonResponse = apicall.POSTCALLBODY(jsonR, url);
                return Json(jsonResponse);
            }
            else
            {
                return Json(response);
            }            
        }
        public IActionResult UPDATE(string jsonR)
        {            
            string usuario = HttpContext.Session.GetString("usuario");
            Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 27);
            GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponseG);
            var jsonResponse = "";
            if (response.Respuesta == 1)
            {
                string url = "Rol/UPDATE";
                jsonResponse = apicall.POSTCALLBODY(jsonR, url);
                return Json(jsonResponse);


            }
            else
            {
                return Json(response);
            }
        }

        public IActionResult GetBYID(string jsonR)
        {
            string url = "Rol/GetByID";            
            var jsonResponse = "";
            jsonResponse = apicall.POSTCALLBODY(jsonR,url);
            return Json(jsonResponse);
        }
        public IActionResult GET(string jsonR)
        {
            string url = "Rol/";            
            var jsonResponse = "";
            jsonResponse = apicall.POSTCALLBODY(jsonR,url);
            return Json(jsonResponse);
        }
    }
}
