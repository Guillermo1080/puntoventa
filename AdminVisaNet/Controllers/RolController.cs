﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AdminVisaNet.Models;
using AdminVisaNet.Models.WSCALL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using POSWebOS.Models.ResponseJson;

namespace AdminVisaNet.Controllers
{
    public class RolController : Controller
    {
        readonly APIcall apicall = new APIcall();
        public RolController()
        {
         
        }

        public ActionResult IndexRol()
        {
            //string usuario = HttpContext.Session.GetString("usuario");
            //Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            //string jsonResponse = apicall.GetPermisosBYIDRoll(usua.IDRoll, usua.IDUsuario);
            //GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponse);
            //if (response.Respuesta == 1)
            //{
            //    string permiso = JsonConvert.SerializeObject(response.DetallePermiso);
            //    HttpContext.Session.SetString("rol", permiso);

            //}
            return View();
        }
        public IActionResult GET(string jsonR)
        {
            string url = "Rol";            
            var jsonResponse = apicall.POSTCALLBODY(jsonR,url);
            return Json(jsonResponse);
        }
        public IActionResult DeleteBYID(string jsonR)
        {            
            string usuario = HttpContext.Session.GetString("usuario");
            Usuario usua = JsonConvert.DeserializeObject<Usuario>(usuario);
            string jsonResponseG = apicall.CheckPermiso(usua.IDUsuario, usua.IDRoll, 26);
            GlobalResponse response = JsonConvert.DeserializeObject<GlobalResponse>(jsonResponseG);    
            if (response.Respuesta == 1)
            {
                string url = "Rol/DELETE/";
                var jsonResponse = apicall.POSTCALLBODY(jsonR,url);
                return Json(jsonResponse);
            }
            else
            {
                return Json(response);
            }
        }
    }
}
