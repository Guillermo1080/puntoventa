﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using POSWebOS.Models;
using POSWebOS.Models.DB;
using POSWebOS.Models.Operations;
using POSWebOS.Models.Reports;
using POSWebOS.Models.ResponseJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Controllers
{
    public class Reportes : Controller
    {
        OperationDB ops;
        private readonly Context _context;
        public ActionResult IndexRepProducto()
        {
            return View();
        }
        public ActionResult IndexRepInventario()
        {
            return View();
        }
        public ActionResult IndexRepProveedor()
        {
            return View();
        }
        public ActionResult IndexRepVenta()
        {
            return View();
        }
        public Reportes(Context context)
        {
            _context = context;
            ops = new OperationDB(_context);
        }
        public IActionResult LoadRepProducto(string jsonR)
        {

            Filters filter = new Filters();
            filter.IDCorte = 0;
            filter.IDEstablecimiento =0;
            filter.FechaFin = DateTime.MinValue;
            filter.FechaIni = DateTime.MinValue;
            filter.IDProveedor=0;
            filter.IDProducto = "";

            List<ReportProducto> lstProd = new List<ReportProducto>();
            lstProd= ops.GetReportProducto(filter);
            string prov = JsonConvert.SerializeObject(lstProd);

            return Json(prov);
        }

        public IActionResult LoadRepProductoByFilters(string jsonR)
        {
            Filters filter = JsonConvert.DeserializeObject<Filters>(jsonR);

            if (filter.FechaFin == null)
            {
                filter.FechaFin = DateTime.MinValue;
            }
            if (filter.FechaIni == null)
            {
                filter.FechaIni = DateTime.MinValue;

            }

            List<ReportProducto> lstProd = new List<ReportProducto>();
            lstProd = ops.GetReportProducto(filter);
            string prov = JsonConvert.SerializeObject(lstProd);

            return Json(prov);
        }

        public IActionResult LoadRepVenta(string jsonR)
        {
            Filters filter = new Filters();
            filter.IDCorte = 0;
            filter.IDEstablecimiento = 0;
            filter.FechaFin = DateTime.MinValue;
            filter.FechaIni = DateTime.MinValue;
            filter.IDProveedor = 0;
            filter.IDProducto = "";

            List<ReportSale> lstSale = new List<ReportSale>();
            lstSale = ops.GetReportSale(filter);
            string ventas = JsonConvert.SerializeObject(lstSale);

            return Json(ventas);
        }
        public IActionResult LoadRepVentaByFilters(string jsonR)
        {
            Filters filter = JsonConvert.DeserializeObject<Filters>(jsonR);

            if (filter.FechaFin == null)
            {
                filter.FechaFin = DateTime.MinValue;
            }
            if (filter.FechaIni == null)
            {
                filter.FechaIni = DateTime.MinValue;

            }
            List<ReportSale> lstSale = new List<ReportSale>();
            lstSale = ops.GetReportSale(filter);
            string ventas = JsonConvert.SerializeObject(lstSale);

            return Json(ventas);
        }


        public IActionResult LoadRepInventario(string jsonR)
        {
            Filters filter = new Filters();
            filter.IDCorte = 0;
            filter.IDEstablecimiento = 0;
            filter.FechaFin = DateTime.MinValue;
            filter.FechaIni = DateTime.MinValue;
            filter.IDProveedor = 0;
            filter.IDProducto = "";

            List<ReportInventario> lstInv = new List<ReportInventario>();
            lstInv = ops.GetReportInventario(filter);
            string inventario = JsonConvert.SerializeObject(lstInv);

            return Json(inventario);
        }

        public IActionResult LoadRepInventarioByFilters(string jsonR)
        {
            Filters filter = JsonConvert.DeserializeObject<Filters>(jsonR);

            if (filter.FechaFin == null)
            {
                filter.FechaFin = DateTime.MinValue;
            }
            if (filter.FechaIni == null)
            {
                filter.FechaIni = DateTime.MinValue;

            }

            List<ReportInventario> lstInv = new List<ReportInventario>();
            lstInv = ops.GetReportInventario(filter);
            string inventario = JsonConvert.SerializeObject(lstInv);

            return Json(inventario);
        }

        public IActionResult LoadRepProveedor(string jsonR)
        {

            Filters filter = new Filters();
            filter.IDCorte = 0;
            filter.IDEstablecimiento = 0;
            filter.FechaFin = DateTime.MinValue;
            filter.FechaIni = DateTime.MinValue;
            filter.IDProveedor = 0;
            filter.IDProducto = "";

            List<ReportProveedor> lstProve = new List<ReportProveedor>();
            lstProve = ops.GetReportProveedor(filter);
            string proveedores = JsonConvert.SerializeObject(lstProve);

            return Json(proveedores);
        }

        public IActionResult LoadRepProveedorByFilters(string jsonR)
        {
            Filters filter = JsonConvert.DeserializeObject<Filters>(jsonR);

            if (filter.FechaFin == null)
            {
                filter.FechaFin = DateTime.MinValue;
            }
            if (filter.FechaIni == null)
            {
                filter.FechaIni = DateTime.MinValue;

            }

            List<ReportProveedor> lstProve = new List<ReportProveedor>();
            lstProve = ops.GetReportProveedor(filter);
            string proveedores = JsonConvert.SerializeObject(lstProve);

            return Json(proveedores);
        }
    }
}
