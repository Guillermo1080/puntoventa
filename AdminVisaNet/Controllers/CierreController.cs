﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using POSWebOS.Models;
using POSWebOS.Models.DB;
using POSWebOS.Models.Operations;
using POSWebOS.Models.RequestJson;
using POSWebOS.Models.ResponseJson;

namespace POSWebOS.Controllers
{
    public class CierreController : Controller
    {
        OperationDB ops;
        private readonly Context _context;
        public IActionResult IndexCierre()
        {
            return View();
        }
        public CierreController(Context context)
        {

            _context = context;
            ops = new OperationDB(_context);
        }
        public IActionResult GetCorte(string jsonR)
        {
            CierreRequest objnit = JsonConvert.DeserializeObject<CierreRequest>(jsonR);            
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel emp = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);
            CorteResponse corte = ops.GetCortesBYIDEmpresa(emp.IDEmpresa,objnit.IDEstablecimiento);
            if(corte.Corte is null)
            {
                GlobalResponse global = new GlobalResponse();                
                global.Descripcion = "No hay cortes abiertos";
                string jsonResponse = JsonConvert.SerializeObject(global);
                return Json(jsonResponse);
            }
            else
            {
                corte.Respuesta = 1;
                string jsonResponse = JsonConvert.SerializeObject(corte);
                return Json(jsonResponse);
            }
            
            
        }
        public IActionResult CloseAdd(string jsonR)
        {
            CierreRequest objnit = JsonConvert.DeserializeObject<CierreRequest>(jsonR);
            string sempresa = HttpContext.Session.GetString("empresa");
            EmpresaModel emp = JsonConvert.DeserializeObject<EmpresaModel>(sempresa);
            string susuario = HttpContext.Session.GetString("usuario");
            UsuarioModel obUsuario = JsonConvert.DeserializeObject<UsuarioModel>(susuario);
            CorteModel cierre = new CorteModel();
            cierre.IDCorte = objnit.IDCorte;
            cierre.IDEmpresa = emp.IDEmpresa;
            cierre.IDUsuario = obUsuario.IDUsuario;
            cierre.Observaciones = objnit.Observaciones;
            cierre.IDEstablecimiento = objnit.IDEstablecimiento;
            if (objnit.IDCorte == 0)
            {
                
                CorteResponse corte = ops.AddCorte(cierre);
                string jsonResponse = JsonConvert.SerializeObject(corte);
                return Json(jsonResponse);
            }
            else {
                CorteResponse corte = ops.EditCorte(cierre);
                string jsonResponse = JsonConvert.SerializeObject(corte);
                return Json(jsonResponse);
            }
            
            //CorteResponse corte = ops.GetCortesBYIDEmpresa(emp.IDEmpresa, objnit.IDEstablecimiento);
            //if (corte.Corte is null)
            //{
            //    GlobalResponse global = new GlobalResponse();
            //    global.Descripcion = "No hay cortes abiertos";
            //    string jsonResponse = JsonConvert.SerializeObject(global);
            //    return Json(jsonResponse);
            //}
            //else
            //{
            //    corte.Respuesta = 1;
            //    string jsonResponse = JsonConvert.SerializeObject(corte);
            //    return Json(jsonResponse);
            //}


        }
    }
}
