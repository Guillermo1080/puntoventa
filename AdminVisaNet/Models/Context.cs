﻿using Microsoft.EntityFrameworkCore;
using POSWebOS.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models
{
    public class Context: DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options)
        {

        }
        public Context()
        {
        }
        public DbSet<AccionModel> AccionDB { get; set; }
        public DbSet<EmpresaModel> EmpresaDB { get; set; }
        public DbSet<EstablecimientoModel> EstablecimientoDB { get; set; }
        public DbSet<UsuarioModel> UsuarioDB { get; set; }
        public DbSet<RolModel> RolDB { get; set; }
        public DbSet<ProveedorModel> ProveedorDB { get; set; }
        public DbSet<CategoriaModel> CategoriaDB { get; set; }
        public DbSet<ClienteModel> ClienteDB { get; set; }
        public DbSet<ProductoModel> ProductoDB { get; set; }
        public DbSet<TransaccionModel> TransaccionDB { get; set; }
        public DbSet<ItemTransaccionModel> ItemTransaccionDB { get; set; }
        public DbSet<FacturaTransaccionModel> FacturaTransaccionDB { get; set; }
        public DbSet<CorteModel> CorteDB { get; set; }
        public DbSet<MovInvProductoModel> MovInvProductoDB { get; set; }
    }
}
