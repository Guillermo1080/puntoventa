﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models
{
    public class Filters
    {
        public int? IDTransaccion { get; set; }
        public int? IDEstablecimiento { get; set; }
        public int? IDCorte { get; set; }
        public string IDProducto { get; set; }
        public int? IDProveedor { get; set; }
        public DateTime? FechaIni { get; set; }
        public DateTime? FechaFin { get; set; }

        public string NIT { get; set; }

        public string Serie { get; set; }


    }
}
