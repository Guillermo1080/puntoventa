﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models
{
    public class Certificacion
    {
        public int RespuestaDigifact = 0;
        public string Mensaje = "";
        public string NITCertificador = "";
        public string NombreCertificador = "";
        public string NumeroAutorizacion = "";
        public string Serie = "";
        public string Numero = "";
        public string xml_signed = "";
        public string xml_nativo = "";
        public string FechaCertificacion = "";
        public string TipoEmision = "";
        public string InvoiceType = "";
        public Certificacion() { }

    }
}
