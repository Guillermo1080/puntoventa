﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.Reports
{
    public class ReportProveedor
    {
        public DateTime? FechaApertura { get; set; }
        public int? IDEmpresa { get; set; }
        public int? IDEstablecimiento { get; set; }
        public int? IDCorte { get; set; }

        public string IDProducto { get; set; }
        public string Nombre { get; set; }
        public decimal? Cantidad { get; set; }

        public int? IDProveedor { get; set; }
        public string Proveedor { get; set; }
    }
}
