﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.DB
{
    public class ReportSale
    {
        public DateTime? FechaEmision { get; set; }
        public int? IDEmpresa { get; set; }
        public int? IDEstablecimiento { get; set; }
        public int? IDCorte { get; set; }
        public decimal? MontoIVA { get; set; }
        public decimal? Monto { get; set; }
        public string? NIT { get; set; }
        public string? Receptor { get; set; }



    }
}
