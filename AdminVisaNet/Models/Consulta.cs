﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models
{
    public class Consulta
    {
        public int? IDTransaccion { get; set; }
        public DateTime? FechaEmision { get; set; }
        public int? IDEmpresa { get; set; }
        public int? IDEstablecimiento { get; set; }
        public int? IDCorte { get; set; }
        public decimal? MontoIVA { get; set; }
        public decimal? Monto { get; set; }
        public string? NIT { get; set; }
        public string? Receptor { get; set; }
        public string? DOCGUID { get; set; }
        public string? Serie { get; set; }


    }
}

