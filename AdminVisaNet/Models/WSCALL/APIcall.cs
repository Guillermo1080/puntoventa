﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AdminVisaNet.Models.WSCALL
{
    public class APIcall
    {
        int timeout;
        public APIcall()
        {

        }

        public int GetValueTimeout()
        {

            try
            {
                List<Configuracion> lstConfig;
                var data = MemoryCache.Default["config"];
                if (data is null)
                {
                    timeout = 10000;
                }
                else
                {
                    lstConfig = JsonConvert.DeserializeObject<List<Configuracion>>(Convert.ToString(data));
                    timeout = Convert.ToInt32((from d in lstConfig
                                               where d.Key == "Timeout"
                                               select d.Value).FirstOrDefault());
                    if (timeout <= 1000) { timeout = 10000; }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR en APIcall, GetValueTimeout: " + ex.ToString());

            }
            return timeout;

        }
        public string POSTCALLBODY(string jsonrequest, string url)
        {
            string WEBSERVICE_URL = @"https://localhost:44370/api/" + url;
            var jsonResponse = "";
            try
            {
                Encoding encoding = new UTF8Encoding();
                byte[] data = encoding.GetBytes(CheckForSQLInjection(jsonrequest));
                var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
                if (webRequest != null)
                {
                    webRequest.Method = "POST";
                    webRequest.Timeout = GetValueTimeout();
                    webRequest.ContentType = "application/json; charset=utf-8";
                    webRequest.Headers.Add("ApiKey", "V1SaN3tAdmin");

                    using (var stream = webRequest.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s, System.Text.Encoding.UTF8, false))
                        {
                            jsonResponse = sr.ReadToEnd();
                            //Console.WriteLine (String.Format("Response: {0}", jsonResponse));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            if (string.IsNullOrEmpty(jsonResponse))
            {
                jsonResponse = ErrorMessage();
            }
            return jsonResponse;

        }
        //public string GETCALL(string url)
        //{

        //    string WEBSERVICE_URL = @"https://localhost:44370/api/" + url;
        //    var jsonResponse = "";
        //    try
        //    {
        //        var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
        //        if (webRequest != null)
        //        {
        //            webRequest.Method = "GET";
        //            webRequest.Timeout = 120000;
        //            webRequest.ContentType = "application/json";
        //            webRequest.Headers.Add("ApiKey", "V1SaN3tAdmin");

        //            using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
        //            {
        //                using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
        //                {
        //                    jsonResponse = sr.ReadToEnd();
        //                    //Console.WriteLine (String.Format("Response: {0}", jsonResponse));
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.ToString());
        //    }
        //    if (string.IsNullOrEmpty(jsonResponse))
        //    {
        //        jsonResponse = ErrorMessage();
        //    }
        //    return jsonResponse;

        //}
        //public string POSTCALL(string url)
        //{

        //    string WEBSERVICE_URL = @"https://localhost:44370/api/" + url;
        //    var jsonResponse = "";
        //    try
        //    {
        //        var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
        //        if (webRequest != null)
        //        {
        //            webRequest.Method = "POST";
        //            webRequest.Timeout = 120000;
        //            webRequest.ContentType = "application/json";
        //            webRequest.Headers.Add("ApiKey", "V1SaN3tAdmin");

        //            using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
        //            {
        //                using (System.IO.StreamReader sr = new System.IO.StreamReader(s, System.Text.Encoding.Default, false))
        //                {
        //                    jsonResponse = sr.ReadToEnd();
        //                    //Console.WriteLine (String.Format("Response: {0}", jsonResponse));
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.ToString());
        //    }
        //    if (string.IsNullOrEmpty(jsonResponse))
        //    {
        //        jsonResponse = ErrorMessage();
        //    }
        //    return jsonResponse;

        //}
        //public string CheckPermiso(string url)
        //{

        //    string WEBSERVICE_URL = @"https://localhost:44370/api/" + url;
        //    var jsonResponse = "";
        //    try
        //    {
        //        var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
        //        if (webRequest != null)
        //        {
        //            webRequest.Method = "POST";
        //            webRequest.Timeout = 120000;
        //            webRequest.ContentType = "application/json";
        //            webRequest.Headers.Add("ApiKey", "V1SaN3tAdmin");

        //            using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
        //            {
        //                using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
        //                {
        //                    jsonResponse = sr.ReadToEnd();
        //                    //Console.WriteLine (String.Format("Response: {0}", jsonResponse));
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.ToString());
        //    }
        //    if (string.IsNullOrEmpty(jsonResponse))
        //    {
        //        jsonResponse = ErrorMessage();
        //    }

        //    return jsonResponse;

        //}

        public string GetPermisosBYIDRoll(int idroll, int idusuario)
        {
            RolPermiso Rol = new RolPermiso();
            Rol.IDRoll = idroll;
            Rol.IDUsuario = idusuario;
            string WEBSERVICE_URL = @"https://localhost:44370/api/" + "Rol/GetByID";
            string jsonrequest = JsonConvert.SerializeObject(Rol);
            var jsonResponse = "";
            try
            {
                Encoding encoding = new UTF8Encoding();
                byte[] data = encoding.GetBytes(CheckForSQLInjection(jsonrequest));
                var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
                if (webRequest != null)
                {
                    webRequest.Method = "POST";
                    webRequest.Timeout = GetValueTimeout();
                    webRequest.ContentType = "application/json; charset=utf-8";
                    webRequest.Headers.Add("ApiKey", "V1SaN3tAdmin");

                    using (var stream = webRequest.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s, System.Text.Encoding.UTF8, false))
                        {
                            jsonResponse = sr.ReadToEnd();
                            //Console.WriteLine (String.Format("Response: {0}", jsonResponse));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            if (string.IsNullOrEmpty(jsonResponse))
            {
                jsonResponse = ErrorMessage();
            }
            return jsonResponse;

        }
        public string CheckPermiso(int idusuario, int idroll, int idpermiso)
        {
            RolPermiso rolP = new RolPermiso();
            rolP.IDUsuario = idusuario;
            rolP.IDRoll = idroll;
            rolP.IDPermiso = idpermiso;
            string jsonrequest = JsonConvert.SerializeObject(rolP);
            string WEBSERVICE_URL = @"https://localhost:44370/api/" + "Rol/AUTHBYID";
            var jsonResponse = "";
            try
            {
                Encoding encoding = new UTF8Encoding();
                byte[] data = encoding.GetBytes(CheckForSQLInjection(jsonrequest));
                var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
                if (webRequest != null)
                {
                    webRequest.Method = "POST";
                    webRequest.Timeout = GetValueTimeout();
                    webRequest.ContentType = "application/json; charset=utf-8";
                    webRequest.Headers.Add("ApiKey", "V1SaN3tAdmin");

                    using (var stream = webRequest.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s, System.Text.Encoding.UTF8, false))
                        {
                            jsonResponse = sr.ReadToEnd();
                            //Console.WriteLine (String.Format("Response: {0}", jsonResponse));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            if (string.IsNullOrEmpty(jsonResponse))
            {
                jsonResponse = ErrorMessage();
            }
            return jsonResponse;

        }

        public string CheckForSQLInjection(string userInput)
        {
            string[] sqlCheckList = { "--",

                                       ";--",

                                       ";",

                                       "/*",

                                       "*/",

                                        "@@",                                        

                                        "char",

                                       "nchar",

                                       "varchar",

                                       "nvarchar",

                                       "alter",

                                       "begin",

                                       "cast",

                                       "create",

                                       "cursor",

                                       "declare",

                                       "delete",

                                       "drop",

                                       "end",

                                       "exec",

                                       "execute",

                                       "fetch",

                                            "insert",

                                          "kill",

                                             "select",

                                           "sys",

                                            "sysobjects",

                                            "syscolumns",

                                           "table",

                                           "update",
                                           "where"

                                       };
            string cleanString = userInput.Replace("'", "''");
            try
            {
                for (int i = 0; i <= sqlCheckList.Length - 1; i++)
                {
                    if ((cleanString.IndexOf(sqlCheckList[i], StringComparison.OrdinalIgnoreCase) >= 0))
                    {
                        string pattern = @"\b" + sqlCheckList[i] + "\\b";
                        cleanString = Regex.Replace(cleanString, pattern, "", RegexOptions.IgnoreCase);
                        Console.WriteLine(cleanString);
                    }
                }
            }
            catch (Exception ex) {
                cleanString = "";
                Console.WriteLine("ERROR APICall, CheckForSQLInjection: " + ex.ToString());
            }
           
                    
            return cleanString;
        }

        public string ErrorMessage()
    {
        string response = "{\"respuesta\":0,\"descripcion\":\"Error de comunicacion con los servicios\"}";

        return response;

    }
}
}

