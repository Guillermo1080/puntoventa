﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSWebOS.Models.WSCALL
{
    public class WSDigifact
    {
        public WSDigifact()
        {

        }
        public string POSTCALLBODY(string nit)
        {
            string jsonrequest = @"{'IDTipoRequest': 0, 'NIT': '"+nit+"', 'RequestorName': '', 'CentroCostoID': '', 'CodigoEstablecimiento': 0, 'EmpresaID': 0}";
            string WEBSERVICE_URL = @"http://localhost:64371/api/GetInfoFiscal";
            var jsonResponse = "";
            try
            {
                Encoding encoding = new UTF8Encoding();
                byte[] data = encoding.GetBytes(jsonrequest);
                var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
                if (webRequest != null)
                {
                    webRequest.Timeout = 100000;
                    webRequest.Method = "POST";                    
                    webRequest.ContentType = "application/json; charset=utf-8";
                    webRequest.Headers.Add("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IlVzdWFyaW8xIiwibmJmIjoxNjA1NzUzNzExLCJleHAiOjE2Njg4MjU3MTEsImlhdCI6MTYwNTc1MzcxMSwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo2NDM3MS8iLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjY0MzcxLyJ9.HFQOqL7ow-I3tu4kdp0l-VDN3vgtH8bC7Egg8Ou6iZ8");
                    webRequest.Headers.Add("Content-Type", "application/json");
                    using (var stream = webRequest.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s, System.Text.Encoding.UTF8, false))
                        {
                            jsonResponse = sr.ReadToEnd();
                            //Console.WriteLine (String.Format("Response: {0}", jsonResponse));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            if (string.IsNullOrEmpty(jsonResponse))
            {
                jsonResponse = "";
            }
            
            return jsonResponse;

        }
    }
}
