﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminVisaNet.Models
{
    public class DetallePermiso
    {
        public int IDEstado { get; set; }
        public int IDPermiso { get; set; }
        public string Componente { get; set; }
        public string Rol { get; set; }
    }
}
