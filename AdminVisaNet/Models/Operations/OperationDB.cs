﻿using AdminVisaNet.Models;
using Microsoft.AspNetCore.Http;
using POSWebOS.Models.DB;
using POSWebOS.Models.Reports;
using POSWebOS.Models.ResponseJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.Operations
{
    public class OperationDB
    {
        private readonly Context _context;
        EmpresaModel empresa;

        public OperationDB(Context context)
        {
            _context = context;
   
        }
        #region GETS

        public List<Consulta> GetConsulta(Filters filter)
        {
            List<Consulta> lstReport = new List<Consulta>();
                        var query = _context.TransaccionDB
                            .Join(
                                _context.FacturaTransaccionDB,
                                trans => trans.IDTransaccion,
                                fact => fact.IDTransaccion,
                                (trans, fact) => new
                                {
                                    IDTransaccion=trans.IDTransaccion,
                                    FechaEmision = trans.FechaEmision,
                                    IDEmpresa = trans.IDEmpresa,
                                    IDEstablecimiento = trans.IDEstablecimiento,
                                    MontoIVA = trans.MontoIVA,
                                    IDCorte = trans.IDCorte,
                                    Monto = trans.Monto,
                                    NIT = trans.NitReceptor,
                                    Receptor = trans.NombreReceptor,
                                    DOCGUID= fact.DocumentGUID,
                                    Serie=fact.Serie
                                });

            if (filter.IDCorte > 0)
                query = query.Where(x => x.IDCorte == filter.IDCorte);

            if (filter.IDEstablecimiento > 0)
                query = query.Where(x => x.IDEstablecimiento == filter.IDEstablecimiento);

            if (filter.IDTransaccion > 0)
                query = query.Where(x => x.IDTransaccion == filter.IDTransaccion);

            if (!string.IsNullOrEmpty(filter.Serie))
                query = query.Where(x => x.Serie == filter.Serie);

            if (!string.IsNullOrEmpty(filter.NIT))
                query = query.Where(x => x.NIT == filter.NIT);

            if (filter.FechaIni != DateTime.MinValue && filter.FechaFin != DateTime.MinValue)
                query = query.Where(obj => obj.FechaEmision >= filter.FechaIni && obj.FechaEmision < filter.FechaFin);


            var sub = query.ToList();

            foreach (var item in sub)
            {
                Consulta sale = new Consulta();
                sale.FechaEmision = item.FechaEmision;
                sale.IDCorte = item.IDCorte;
                sale.IDEstablecimiento = item.IDEstablecimiento;
                sale.Monto = item.Monto;
                sale.MontoIVA = item.MontoIVA;
                sale.Receptor = item.Receptor;
                sale.NIT = item.NIT;
                sale.IDEmpresa = item.IDEmpresa;
                sale.DOCGUID = item.DOCGUID;
                sale.Serie = item.Serie;
                sale.IDTransaccion=item.IDTransaccion;
                lstReport.Add(sale);
            }

            return lstReport;

        }
        public List<ProductoModel> GetStockBYID(int idemp)
        {
            
            CorteResponse response = new CorteResponse();            
            var query = _context.CorteDB
                            .Join(
                                _context.MovInvProductoDB,
                                corte => corte.IDCorte,
                                mov => mov.IDCorte,
                                (corte, mov) => new
                                {
                                    IDEmpresa = corte.IDEmpresa,
                                    IDEstablecimiento = corte.IDEstablecimiento,
                                    IDProducto = mov.IDProducto,
                                    Cantidad = mov.Cantidad
                                }            
                            ).Where(x => x.IDEmpresa == idemp)                                
                                .ToList();
            List<ProductoModel> lstProd = new List<ProductoModel>();
            foreach (var item in query) {
                ProductoModel prod = new ProductoModel();
                prod.Cantidad = item.Cantidad;
                prod.IDProducto = item.IDProducto;
                lstProd.Add(prod);
             }
            return lstProd;
            


        }
        public List<ReportSale> GetReportSale(Filters filter)
        {
            List<ReportSale> lstReport = new List<ReportSale>();
            CorteResponse response = new CorteResponse();
            var query = _context.TransaccionDB
                            .Join(
                                _context.FacturaTransaccionDB,
                                trans => trans.IDTransaccion,
                                fact => fact.IDTransaccion,
                                (trans, fact) => new
                                {
                                    FechaEmision=trans.FechaEmision,
                                    IDEmpresa = trans.IDEmpresa,
                                    IDEstablecimiento = trans.IDEstablecimiento,
                                    MontoIVA = trans.MontoIVA,
                                    IDCorte=trans.IDCorte,
                                    Monto=trans.Monto,
                                    NIT=trans.NitReceptor,
                                    Receptor=trans.NombreReceptor
                                });

            if (filter.IDCorte > 0)
                query = query.Where(x => x.IDCorte == filter.IDCorte);

            if (filter.IDEstablecimiento>0)
                query = query.Where(x => x.IDEstablecimiento == filter.IDEstablecimiento);

            if (filter.FechaIni!=DateTime.MinValue && filter.FechaFin!=DateTime.MinValue)
                query = query.Where(obj => obj.FechaEmision >= filter.FechaIni && obj.FechaEmision < filter.FechaFin);


            var sub = query.ToList();

            foreach(var item in sub)
            {
                ReportSale sale = new ReportSale();
                sale.FechaEmision = item.FechaEmision;
                sale.IDCorte = item.IDCorte;
                sale.IDEstablecimiento = item.IDEstablecimiento;
                sale.Monto = item.Monto;
                sale.MontoIVA = item.MontoIVA;
                sale.Receptor = item.Receptor;
                sale.NIT = item.Receptor;
                sale.IDEmpresa = item.IDEmpresa;
                lstReport.Add(sale);
            }

            return lstReport;

        }
        public List<ReportProducto> GetReportProducto(Filters filter)
        {
            List<ReportProducto> lstProd = new List<ReportProducto>();
            var query = _context.TransaccionDB
                            .Join(
                                _context.ItemTransaccionDB,
                                trans => trans.IDTransaccion,
                                item => item.IDTransaccion,
                                (trans, item) => new
                                {
                                    FechaEmision = trans.FechaEmision,
                                    IDEmpresa = trans.IDEmpresa,
                                    IDEstablecimiento = trans.IDEstablecimiento,                                    
                                    IDCorte = trans.IDCorte,                                    
                                    IDProducto = item.IDProducto,
                                    Nombre=item.Nombre,
                                    Cantidad =item.Cantidad,
                                    Descuento=item.Descuento,
                                    PrecioUnitario = item.PrecioUnitario,
                                    PrecioCosto = item.PrecioCosto
                                });

            if (filter.IDCorte > 0)
                query = query.Where(x => x.IDCorte == filter.IDCorte);

            if (filter.IDEstablecimiento > 0)
                query = query.Where(x => x.IDEstablecimiento == filter.IDEstablecimiento);

            if (filter.FechaIni != DateTime.MinValue && filter.FechaFin != DateTime.MinValue)
                query = query.Where(obj => obj.FechaEmision >= filter.FechaIni && obj.FechaEmision < filter.FechaFin);

            var sub = query.ToList();
            foreach(var item in sub)
            {
                ReportProducto prod = new ReportProducto();
                prod.IDCorte = item.IDCorte;
                prod.FechaEmision = item.FechaEmision;
                prod.IDEmpresa = item.IDEmpresa;
                prod.IDProducto = item.IDProducto;
                prod.Nombre = item.Nombre;
                prod.PrecioCosto = item.PrecioCosto;
                prod.PrecioUnitario = item.PrecioUnitario;
                prod.Descuento = item.Descuento;
                prod.Cantidad = item.Cantidad;
                prod.IDEstablecimiento = item.IDEstablecimiento;
                lstProd.Add(prod);
            }

            return lstProd;



        }

        public List<ReportInventario> GetReportInventario(Filters filter)
        {
            List<ReportInventario> lstInv = new List<ReportInventario>();            
            var query = _context.CorteDB
                            .Join(
                                _context.MovInvProductoDB,
                                corte => corte.IDCorte,
                                mov => mov.IDCorte,
                                (corte, mov) => new
                                {
                                    IDCorte= corte.IDCorte,
                                    IDEmpresa = corte.IDEmpresa,
                                    IDEstablecimiento = corte.IDEstablecimiento,
                                    IDProducto = mov.IDProducto,
                                    Cantidad = mov.Cantidad
                                });
            //query = query.Where(x => x.IDCorte == filter.IDCorte);

            if (filter.IDCorte>0)
                query = query.Where(x => x.IDCorte == filter.IDCorte);

            if (filter.IDEstablecimiento > 0)
                query = query.Where(x => x.IDEstablecimiento == filter.IDEstablecimiento);

            if (!string.IsNullOrEmpty(filter.IDProducto))
                query = query.Where(x => x.IDProducto == filter.IDProducto);

            var sub=query.ToList();

            //List<ProductoModel> stock = GetStockBYID(1);

            List<string> lstProd = new List<string>();

            foreach (var prod in sub)
            {

                lstProd.Add(prod.IDProducto);
            }

            foreach (var prod in sub)
            {
                ReportInventario inv = new ReportInventario();
                inv.IDCorte = prod.IDCorte;
                inv.IDEmpresa = prod.IDEmpresa;
                inv.IDEstablecimiento = prod.IDEstablecimiento;
                inv.IDProducto = prod.IDProducto;
                inv.Nombre = GetPProductoNameBYID(prod.IDProducto);
                inv.Cantidad= sub.Where(t => t.IDProducto == prod.IDProducto).Sum(i => i.Cantidad);
                lstInv.Add(inv);
            }
            
            List<ReportInventario> lstResponse = new List<ReportInventario>();

            foreach(ReportInventario rep in lstInv)
            {
                ReportInventario aa = lstResponse.Where(t => t.IDProducto == rep.IDProducto).FirstOrDefault();
                if (aa is null)
                {
                    
                    lstResponse.Add(rep);

                }
            }


            return lstResponse;
        }
        public List<ReportProveedor> GetReportProveedor(Filters filter)
        {
            List<ReportProveedor> lstProv = new List<ReportProveedor>();
            CorteResponse response = new CorteResponse();
            var query = _context.CorteDB
                            .Join(
                                _context.MovInvProductoDB,
                                corte => corte.IDCorte,
                                mov => mov.IDCorte,
                                (corte, mov) => new
                                {
                                    FechaApertura=corte.FechaApertura,
                                    IDCorte=corte.IDCorte,
                                    IDEmpresa = corte.IDEmpresa,
                                    IDEstablecimiento = corte.IDEstablecimiento,
                                    IDProducto = mov.IDProducto,
                                    Cantidad = mov.Cantidad,
                                    IDProveedor=mov.IDProveedor,
                                    IDTipoMovProducto=mov.IDTipoMovProducto

                                });
            query = query.Where(x => x.IDTipoMovProducto == 1);
            
            if (filter.IDCorte > 0)
                query = query.Where(x => x.IDCorte == filter.IDCorte);
            //GetProveedorNameBYID
            if (filter.IDProveedor>0)
                query = query.Where(x => x.IDProveedor == filter.IDProveedor);
             
            var sub = query.ToList();
            foreach(var item in sub)
            {
                ReportProveedor prov = new ReportProveedor();
                prov.FechaApertura = item.FechaApertura;
                prov.Cantidad = item.Cantidad;
                prov.IDCorte = item.IDCorte;
                prov.IDEmpresa = item.IDEmpresa;
                prov.IDEstablecimiento = item.IDEstablecimiento;
                prov.IDProducto = item.IDProducto;
                prov.Nombre = GetPProductoNameBYID(item.IDProducto);
                prov.IDProveedor = item.IDProveedor;
                prov.Proveedor = GetProveedorNameBYID(item.IDProveedor);
                lstProv.Add(prov);
                
            }

            return lstProv;
        }
        public CorteResponse GetCortesBYIDEmpresa(int idempresa,int idesta)
        {
            CorteResponse response = new CorteResponse();
            response.Corte =
                        _context.CorteDB.Where(d => d.IDEmpresa == idempresa && d.IDEstablecimiento == idesta && d.IDEstado==1).OrderByDescending(d=>d.FechaApertura).FirstOrDefault();
            return response;
        }

        public ProductoModel GetProductoBYID(string idProducto)
        {
            ProductoModel lstProductos =
                        _context.ProductoDB.Where(d => d.IDProducto == idProducto).FirstOrDefault();
            return lstProductos;
        }
        public string GetProveedorNameBYID(int idProv)
        {
            ProveedorModel prov =
                        _context.ProveedorDB.Where(d => d.IDProveedor == idProv).FirstOrDefault();
            return prov.Nombre;
        }
        public string GetPProductoNameBYID(string idprod)
        {
            ProductoModel prod =
                        _context.ProductoDB.Where(d => d.IDProducto == idprod).FirstOrDefault();
            return prod.Nombre;
        }
        public EmpresaModel GetEmpresaBYIDEmpresa(string nit)
        {
            EmpresaModel emp = new EmpresaModel();
            emp = _context.EmpresaDB.Where(d => d.NIT == nit).FirstOrDefault();
            return emp;
        }
        public UsuarioModel GetLogin(string user,string pass)
        {
            UsuarioModel usua = _context.UsuarioDB.Where(d => d.User == user && d.Password == pass).FirstOrDefault();
            return usua;
        }
        public List<UsuarioModel> GetUsuariosBYIDEmpresa(int idempresa)
        {
            List<UsuarioModel> lstUsuarios =
                        _context.UsuarioDB.Where(d => d.IDEmpresa == idempresa).ToList();
            return lstUsuarios;
        }
        public List<EstablecimientoModel> GetEstablecimientosBYIDEmpresa(int idempresa)
        {
            List<EstablecimientoModel> lstEstablecimientos =
                        _context.EstablecimientoDB.Where(d => d.IDEmpresa == idempresa).OrderByDescending(x => x.CodigoSAT).ToList();
            return lstEstablecimientos;
        }
        public List<ProveedorModel> GetProveedoresBYIDEmpresa(int idempresa)
        {
            List<ProveedorModel> lstProveedores =
                        _context.ProveedorDB.Where(d => d.IDEmpresa == idempresa).ToList();
            return lstProveedores;
        }
        public List<CategoriaModel> GetCategoriaBYIDEmpresa(int idempresa)
        {
            List<CategoriaModel> lstCategoria =
                        _context.CategoriaDB.Where(d => d.IDEmpresa == idempresa).ToList();
            return lstCategoria;
        }
        public List<ClienteModel> GetClientesBYIDEmpresa(int idempresa)
        {
            List<ClienteModel> lstClientes =
                        _context.ClienteDB.Where(d => d.IDEmpresa == idempresa).ToList();
            return lstClientes;
        }
        public List<ProductoModel> GetProductosBYIDEmpresa(int idempresa)
        {
            List<ProductoModel> lstProductos =
                        _context.ProductoDB.Where(d => d.IDEmpresa == idempresa).ToList();
            List<ProductoModel> stock = GetStockBYID(idempresa);
            
            
            foreach(ProductoModel prod in lstProductos)
            {
                prod.Cantidad = stock.Where(t => t.IDProducto == prod.IDProducto).Sum(i => i.Cantidad);
            }
            return lstProductos;
        }
        public List<RolModel> GetRolesYIDEmpresa(int idempresa)
        {
            List<RolModel> lstRoles =
                        _context.RolDB.Where(d => d.IDEmpresa == idempresa).ToList();
            return lstRoles;
        }
        #endregion
        #region ADD

        public MovInvProductoResponse AddMovInvProducto(MovInvProductoModel obj)
        {
            MovInvProductoResponse response = new MovInvProductoResponse();
            try
            {
                
                obj.Fecha = DateTime.Now;
                _context.MovInvProductoDB.Add(obj);
                _context.SaveChanges();
                response.Respuesta = 1;
                response.Descripcion = "Inventario creado exitosamente";
                response.MovInvProducto = obj;
            }
            catch (Exception ex)
            {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;

        }

        public FacturaTransaccionResponse AddFacturaTransaccion(FacturaTransaccionModel fact)
        {
            FacturaTransaccionResponse response = new FacturaTransaccionResponse();
            try
            {
                fact.FechaCertificacion = DateTime.Now;
                _context.FacturaTransaccionDB.Add(fact);
                _context.SaveChanges();
                response.Respuesta = 1;
                response.Descripcion = "Venta agreado exitosamente";
                response.FacturaTransaccion = fact;
            }
            catch (Exception ex)
            {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;

        }
        public CorteResponse AddCorte(CorteModel coorte)
        {
            CorteResponse response = new CorteResponse();
            try
            {
                coorte.IDEstado = 1;
                coorte.FechaApertura = DateTime.Now;
                _context.CorteDB.Add(coorte);
                _context.SaveChanges();
                response.Respuesta = 1;
                response.Descripcion = "Corte agreado exitosamente";
                
            }
            catch (Exception ex)
            {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;

        }
        public ClienteResponse AddCliente(ClienteModel clie)
        {
            ClienteResponse response = new ClienteResponse();
            try
            {                
                _context.ClienteDB.Add(clie);
                _context.SaveChanges();
                response.Respuesta = 1;
                response.Descripcion = "Cliente agreado exitosamente";
                response.Cliente = GetClientesBYIDEmpresa(clie.IDEmpresa);
            }
            catch (Exception ex) {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;
           
        }
        public FacturaResponse AddTransaccion(TransaccionModel trans)
        {
            FacturaResponse response = new FacturaResponse();
            try
            {
                trans.FechaEmision = DateTime.Now;
                _context.TransaccionDB.Add(trans);
                _context.SaveChanges();
                response.Respuesta = 1;
                response.Descripcion = "Facturado exitosamente, el ID de tu transaccion es: "+ trans.IDTransaccion;
                response.IDTransaccion = trans.IDTransaccion;
                
            }
            catch (Exception ex)
            {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;

        }

        public FacturaResponse AddItemTrans(ItemTransaccionModel trans)
        {
            FacturaResponse response = new FacturaResponse();
            try
            {
                
                _context.ItemTransaccionDB.Add(trans);
                _context.SaveChanges();
                response.Respuesta = 1;
                response.Descripcion = "Facturado exitosamente";                

            }
            catch (Exception ex)
            {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;

        }
        public ProductoResponse AddProducto(ProductoModel prod)
        {
            ProductoResponse response = new ProductoResponse();
            try
            {
                prod.IDEstado = 1;
                prod.FechaCreacion = DateTime.Now;
                _context.ProductoDB.Add(prod);
                _context.SaveChanges();
                response.Respuesta = 1;
                response.Descripcion = "Producto agreado exitosamente";
                response.Producto = GetProductosBYIDEmpresa(prod.IDEmpresa);
            }
            catch (Exception ex)
            {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;

        }
        public ProveedorResponse AddProveedor(ProveedorModel prov)
        {
            ProveedorResponse response = new ProveedorResponse();
            try
            {
                prov.FechaCreacion = DateTime.Now;
                _context.ProveedorDB.Add(prov);
                _context.SaveChanges();
                response.Respuesta = 1;
                response.Descripcion = "Proveedor agreado exitosamente";
                response.Proveedor = GetProveedoresBYIDEmpresa(prov.IDEmpresa);
            }
            catch (Exception ex)
            {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;

        }
        public CategoriaResponse AddCategoria(CategoriaModel cat)
        {
            CategoriaResponse response = new CategoriaResponse();
            try
            {
                cat.IDEstado = 1;
                _context.CategoriaDB.Add(cat);
                _context.SaveChanges();
                response.Respuesta = 1;
                response.Descripcion = "Categoria agregada exitosamente";
                response.Categoria = GetCategoriaBYIDEmpresa(cat.IDEmpresa);
            }
            catch (Exception ex)
            {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;

        }
        #endregion
        #region EDIT
        public CorteResponse AddFacturaTransaccion(CorteModel coorte)
        {
            CorteResponse response = new CorteResponse();
            try
            {
                coorte.IDCorte = 1;
                coorte.FechaApertura = DateTime.Now;
                _context.CorteDB.Add(coorte);
                _context.SaveChanges();
                response.Respuesta = 1;
                response.Descripcion = "Corte agreado exitosamente";

            }
            catch (Exception ex)
            {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;

        }
        public ProductoResponse EditProducto(ProductoModel prod)
        {
            ProductoResponse response = new ProductoResponse();
            try
            {
                prod.IDEstado = 1;
                _context.ProductoDB.Update(prod);
                _context.SaveChanges();
                response.Respuesta = 1;
                response.Descripcion = "Producto actualizado exitosamente";
                response.Producto = GetProductosBYIDEmpresa(prod.IDEmpresa);
            }
            catch (Exception ex)
            {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;

        }
        public ClienteResponse EditCliente(ClienteModel clie)
        {
            ClienteResponse response = new ClienteResponse();
            ClienteModel cliente;
            try
            {
                cliente = _context.ClienteDB.Where(d => d.IDCliente == clie.IDCliente).FirstOrDefault();
                if (cliente != null)
                {
                    cliente.IDCliente = clie.IDCliente;
                    cliente.Nombre = clie.Nombre;
                    cliente.Apellido = clie.Apellido;
                    cliente.NIT = clie.NIT;
                    cliente.NombreComercial = clie.NombreComercial;
                    cliente.Direccion = clie.Direccion;
                    cliente.Telefono = clie.Telefono;
                    cliente.Email = clie.Email;
                    cliente.IDEmpresa = clie.IDEmpresa;

                    _context.SaveChanges();
                    response.Respuesta = 1;
                    response.Descripcion = "Cliente editado exitosamente";
                    response.Cliente = GetClientesBYIDEmpresa(clie.IDEmpresa);
                }
                else
                {
                    response.Descripcion = "Hubo un error, intente nuevamente";
                }
                
            }
            catch (Exception ex)
            {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;

        }
        public ProveedorResponse EditProveedor(ProveedorModel prov)
        {
            ProveedorResponse response = new ProveedorResponse();
            ProveedorModel obj = new ProveedorModel();
            try
            {
                obj = _context.ProveedorDB.Where(d => d.IDProveedor == prov.IDProveedor).FirstOrDefault();
                if (obj != null)
                {
                    obj.IDProveedor = prov.IDProveedor;
                    obj.Nombre = prov.Nombre;
                    obj.NIT = prov.NIT;
                    obj.Direccion = prov.Direccion;
                    obj.Telefono = prov.Telefono;
                    obj.Email = prov.Email;
                    obj.IDEmpresa = prov.IDEmpresa;

                    _context.SaveChanges();
                    response.Respuesta = 1;
                    response.Descripcion = "Proveedor editado exitosamente";
                    response.Proveedor = GetProveedoresBYIDEmpresa(prov.IDEmpresa);
                }
                else
                {
                    response.Descripcion = "Hubo un error, intente nuevamente";
                }                
            
            }
            catch (Exception ex)
            {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;

        }
        public CategoriaResponse EditCategoria(CategoriaModel cat)
        {
            CategoriaResponse response = new CategoriaResponse();
            CategoriaModel obj = new CategoriaModel();
            try
            {
                obj = _context.CategoriaDB.Where(d => d.IDCategoria == cat.IDCategoria).FirstOrDefault();
                if (obj != null)
                {
                    obj.IDCategoria = cat.IDCategoria;
                    obj.Categoria = cat.Categoria;
                    obj.Observaciones = cat.Observaciones;
                    _context.SaveChanges();
                    response.Respuesta = 1;
                    response.Descripcion = "Categoria editada exitosamente";
                    response.Categoria = GetCategoriaBYIDEmpresa(cat.IDEmpresa);
                }
                else
                {
                    response.Descripcion = "Hubo un error, intente nuevamente";
                }

            }
            catch (Exception ex)
            {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;

        }
        public CorteResponse EditCorte(CorteModel cort)
        {
            CorteResponse response = new CorteResponse();
            CorteModel obj = new CorteModel();
            try
            {
                obj = _context.CorteDB.Where(d => d.IDCorte == cort.IDCorte).FirstOrDefault();
                if (obj != null)
                {
                    obj.IDEstado = 0;
                    obj.FechaCierre = DateTime.Now;                    
                    _context.SaveChanges();
                    response.Respuesta = 1;
                    response.Descripcion = "Corte cerrado exitosamente";
                    
                }
                else
                {
                    response.Descripcion = "Hubo un error, intente nuevamente";
                }

            }
            catch (Exception ex)
            {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;

        }
        #endregion
        #region DELETE
        public ProveedorResponse DeleteProveedor(ProveedorModel prov)
        {
            ProveedorResponse response = new ProveedorResponse();
            ProveedorModel obj = new ProveedorModel();
            try
            {
                obj = _context.ProveedorDB.Where(d => d.IDProveedor == prov.IDProveedor).FirstOrDefault();
                if (obj != null)
                {
                    _context.Remove(obj);                   

                    _context.SaveChanges();
                    response.Respuesta = 1;
                    response.Descripcion = "Proveedor eliminado exitosamente";
                    response.Proveedor = GetProveedoresBYIDEmpresa(prov.IDEmpresa);
                }
                else
                {
                    response.Descripcion = "Hubo un error, intente nuevamente";
                }

            }
            catch (Exception ex)
            {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;

        }
        public ProductoResponse DeleteProducto(ProductoModel prov)
        {
            ProductoResponse response = new ProductoResponse();
            ProductoModel obj = new ProductoModel();
            try
            {
                obj = _context.ProductoDB.Where(d => d.IDProducto == prov.IDProducto).FirstOrDefault();
                if (obj != null)
                {
                    _context.Remove(obj);

                    _context.SaveChanges();
                    response.Respuesta = 1;
                    response.Descripcion = "Producto eliminado exitosamente";
                    response.Producto = GetProductosBYIDEmpresa(prov.IDEmpresa);
                    response.Categoria = GetCategoriaBYIDEmpresa(prov.IDEmpresa);
                }
                else
                {
                    response.Descripcion = "Hubo un error, intente nuevamente";
                }

            }
            catch (Exception ex)
            {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;

        }
        public CategoriaResponse DeleteCategoria(CategoriaModel categoria)
        {
            CategoriaResponse response = new CategoriaResponse();
            CategoriaModel obj = new CategoriaModel();
            try
            {
                obj = _context.CategoriaDB.Where(d => d.IDCategoria == categoria.IDCategoria).FirstOrDefault();
                if (obj != null)
                {
                    _context.Remove(obj);

                    _context.SaveChanges();
                    response.Respuesta = 1;
                    response.Descripcion = "Categoría eliminada exitosamente";
                    response.Categoria = GetCategoriaBYIDEmpresa(categoria.IDEmpresa);
                }
                else
                {
                    response.Descripcion = "Hubo un error, intente nuevamente";
                }

            }
            catch (Exception ex)
            {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;

        }

        public ClienteResponse DeleteCliente(ClienteModel cliente)
        {
            ClienteResponse response = new ClienteResponse();
            ClienteModel obj = new ClienteModel();
            try
            {
                obj = _context.ClienteDB.Where(d => d.IDCliente == cliente.IDCliente).FirstOrDefault();
                if (obj != null)
                {
                    _context.Remove(obj);

                    _context.SaveChanges();
                    response.Respuesta = 1;
                    response.Descripcion = "Cliente eliminado exitosamente";
                    response.Cliente = GetClientesBYIDEmpresa(cliente.IDEmpresa);
                }
                else
                {
                    response.Descripcion = "Hubo un error, intente nuevamente";
                }

            }
            catch (Exception ex)
            {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;

        }
        public UsuarioResponse DeleteUsuario(UsuarioModel usu)
        {
            UsuarioResponse response = new UsuarioResponse();
            UsuarioModel obj = new UsuarioModel();
            try
            {
                obj = _context.UsuarioDB.Where(d => d.IDUsuario == usu.IDUsuario).FirstOrDefault();
                if (obj != null)
                {
                    _context.Remove(obj);

                    _context.SaveChanges();
                    response.Respuesta = 1;
                    response.Descripcion = "Usuario eliminado exitosamente";
                    response.Usuario = GetUsuariosBYIDEmpresa(usu.IDEmpresa);
                }
                else
                {
                    response.Descripcion = "Hubo un error, intente nuevamente";
                }

            }
            catch (Exception ex)
            {
                response.Descripcion = "Hubo un error, intente nuevamente";
            }
            return response;

        }
        #endregion
        
    }
}
