﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models
{
    public class Inventario
    {
        public int No { get; set; }
        public int IDProveedor { get; set; }
        public string Observaciones { get; set; }
        public string IDProducto { get; set; }
        public int Cantidad { get; set; }
        public string Documento  {get; set; }
        public int IDEstablecimiento { get; set; }


    }
}
