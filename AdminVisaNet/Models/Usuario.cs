﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminVisaNet.Models
{
    public class Usuario
    {
        public Int32 IDUsuario { get; set; }
        public String Nombre { get; set; }
        public String Apellido { get; set; }
        public String User { get; set; }
        public String Password { get; set; }
        public String Email { get; set; }
        public Int32 IDEstado { get; set; }
        public Int32 TempPass { get; set; }        
        public DateTime FechaCreacion { get; set; }
        public Int32 IDRoll { get; set; }
        public String OLDPassword { get; set; }
        public Usuario()
        {
        }
    }
}
