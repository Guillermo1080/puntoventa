﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.ResponseJson
{
    public class GlobalResponse
    {
        public int Respuesta = 0;
        public string Descripcion { get; set; }
    }
}
