﻿using POSWebOS.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.ResponseJson
{
    public class MovInvProductoResponse
    {
        public int Respuesta = 0;
        public string Descripcion { get; set; }
        public MovInvProductoModel MovInvProducto { get; set; }
    }
}
