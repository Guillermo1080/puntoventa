﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.ResponseJson
{
    public class WSResponse
    {
        int _Respuesta = 0;
        string _Descripcion = "";
        Receptor _Receptor = new Receptor();

        public int Respuesta { get => _Respuesta; set => _Respuesta = value; }
        public string Descripcion { get => _Descripcion; set => _Descripcion = value; }
        public Receptor Receptor { get => _Receptor; set => _Receptor = value; }
    }
    public class Receptor
    {
        public string NITReceptor = "";
        public string NombreReceptor = "";
        public string Direccion = "CIUDAD";
        public string CodigoPostal = "01002";
        public string Departamento = "GUATEMALA";
        public string Municipio = "GUATEMALA";
        public string Pais = "GT";
    }
}
