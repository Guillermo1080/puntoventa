﻿using POSWebOS.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.ResponseJson
{
    public class FacturaResponse
    {
        public int Respuesta = 0;        
        public string Descripcion { get; set; }
        public int IDTransaccion { get; set; }
        public string PDF { get; set; }
        public string DOCGUID { get; set; }
        public FacturaTransaccionModel Factura { get; set; }
    }
}
