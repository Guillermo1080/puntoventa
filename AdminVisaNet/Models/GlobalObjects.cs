﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminVisaNet.Models
{
    public class GlobalObjects
    {
        public List<DetallePermiso> RolDetalle { get; set; }
        public Usuario usuario { get; set; }

        public GlobalObjects() { }
    }
}
