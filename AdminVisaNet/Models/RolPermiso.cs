﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminVisaNet.Models
{
    public class RolPermiso
    {
        public Int32 IDRoll { get; set; }
        public Int32 IDPermiso { get; set; }
        public Int32 IDUsuario { get; set; }
        public RolPermiso() { }
    }
}
