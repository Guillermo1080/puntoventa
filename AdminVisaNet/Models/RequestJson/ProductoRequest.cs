﻿using POSWebOS.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.RequestJson
{
    public class ProductoRequest
    {

        public ProductoModel Producto { get; set; }
        public List<ProductoModel> Productos { get; set; }
        public MovInvProductoModel MovInvProducto { get; set; }
        public ProductoRequest() { }
    }
}
