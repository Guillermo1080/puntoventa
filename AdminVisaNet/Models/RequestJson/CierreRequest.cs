﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.RequestJson
{
    public class CierreRequest
    {
        public CierreRequest() { }
        public int IDEstablecimiento = 0;
        public int IDCorte = 0;
        public string Observaciones = "";
    }
}
