﻿using POSWebOS.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.RequestJson
{
    public class FacturaRequest
    {
        public TransaccionModel Transaccion { get; set; }
        public List<ProductoModel> Producto { get; set; }
        public string Email { get; set; }

        public FacturaRequest() { }
    }
}
