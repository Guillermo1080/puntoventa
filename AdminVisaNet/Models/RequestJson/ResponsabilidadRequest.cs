﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminVisaNet.Models.RequestJson
{
    public class ResponsabilidadRequest
    {
        public Int32 IDResponsabilidad { get; set; }
        public String Titulo { get; set; }
        public String Descripcion { get; set; }
        public String Img { get; set; }
    }
}
