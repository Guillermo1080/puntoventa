﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.RequestJson
{
    public class LoginRequest
    {
        public string User { get; set; }
        public string Pass { get; set; }
        public string NIT { get; set; }
    }
}
