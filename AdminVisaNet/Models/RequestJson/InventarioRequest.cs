﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.RequestJson
{
    public class InventarioRequest
    {
        public List<Inventario> Inventario { get; set; }


        public InventarioRequest() { }
    }
}
