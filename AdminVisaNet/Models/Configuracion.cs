﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminVisaNet.Models
{
    public class Configuracion
    {
      
        public Int32 IDConfig { get; set; }
        public String Key { get; set; }
        public String Value { get; set; }

        public String Description { get; set; }
     
        public Int32 IDUsuario { get; set; }
    }
}
