﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.DB
{
    [Table("Usuario")]
    public class UsuarioModel
    {
        [Key]
        public Int32 IDUsuario { get; set; }

        public string Nombre { get; set; }

        public string Apellido { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

        public Int32 TempPass { get; set; }
        public Int32 IDEstado { get; set; }
        public Int32 IDEmpresa { get; set; }
        public Int32 IDRoll { get; set; }

        
        public UsuarioModel()
        {            
        }

    }
}
