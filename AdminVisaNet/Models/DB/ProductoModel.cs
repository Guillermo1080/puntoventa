﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.DB
{
    [Table("Producto")]
    public class ProductoModel
    {
        [Key]
        public string IDProducto { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Observaciones { get; set; }
        public string CodigoEAN { get; set; }
        public string Imagen { get; set; }
        public decimal PrecioCosto { get; set; }
        public decimal PrecioVenta { get; set; }
        [NotMapped]
        public int No { get; set; }
        [NotMapped]
        public string ImagenData { get; set; }
        [NotMapped]
        public decimal Cantidad { get; set; }
        [NotMapped]
        public double Subtotal { get; set; }
        public decimal PrecioMinimo { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaVencimiento { get; set; }
        public Int32 IDEstado { get; set; }
        public Int32 IDEstablecimiento { get; set; }
        public Int32 IDCategoria { get; set; }
        public Int32 IDEmpresa { get; set; }

        public ProductoModel()
        {

        }


    }
}
