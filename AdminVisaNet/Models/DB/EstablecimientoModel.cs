﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.DB
{
    [Table("Establecimiento")]
    public class EstablecimientoModel
    {
        [Key]
        public Int32 IDEstablecimiento { get; set; }
        public string NombreComercial { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Departamento { get; set; }
        public string Municipio { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public Int32 CodigoSAT { get; set; }
        public Int32 IDEstado { get; set; }

        public Int32 IDEmpresa { get; set; }

        public EstablecimientoModel()
        {

        }
    }
}
