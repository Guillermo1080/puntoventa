﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.DB
{
    [Table("ItemTransaccion")]
    public class ItemTransaccionModel
    {
        [Key]
        public Int32 IDItemTransaccion { get; set; }
        public decimal Cantidad { get; set; }
        public string IDProducto { get; set; }
        public Int32 IDCategoria { get; set; }
        public decimal PrecioUnitario { get; set; }
        public decimal MontoFacturado { get; set; }
        public decimal? PrecioCosto { get; set; }

        public Int32 IDTransaccion { get; set; }
        public string Nombre { get; set; }
        public decimal Descuento { get; set; }

        public ItemTransaccionModel() { }
    }
}
