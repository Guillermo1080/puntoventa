﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.DB
{
    [Table("facturatransaccion")]
    public class FacturaTransaccionModel
    {
        [Key]
        public Int32 IDFacturaTransaccion { get; set; }
        public DateTime? FechaCertificacion { get; set; }
        public Int32 IDTransaccion { get; set; }
        public string DocumentGUID { get; set; }
        public string Serie { get; set; }
        public string NumDoc { get; set; }
        public string XML_Nativo { get; set; }
        public string XML_Signed { get; set; }
        public bool Contingencia { get; set; }
        public string InvoiceType { get; set; }
        public string NumeroAcceso { get; set; }

        public string RefInterna { get; set; }
        
        public FacturaTransaccionModel()
        {

        }
    }
}
