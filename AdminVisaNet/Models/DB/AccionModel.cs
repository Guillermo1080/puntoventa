﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.DB
{
    [Table("accion")]
    public class AccionModel
    {
        [Key]
        public Int32 IDAccion { get; set; }      

        public string Nombre { get; set; }
        
        public AccionModel()
        {
        }
    }
}
