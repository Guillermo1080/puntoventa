﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.DB
{
    [Table("Categoria")]
    public class CategoriaModel
    {
        [Key]
        public Int32 IDCategoria { get; set; }
        public string Categoria { get; set; }
        public string Observaciones { get; set; }
        public Int32 IDEstado { get; set; }
        public Int32 IDEmpresa { get; set; }
        public CategoriaModel() { 
            
        }
    }
}
