﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.DB
{
    [Table("Cliente")]
    public class ClienteModel
    {
        [Key]
        public Int32 IDCliente { get; set; }

        public string Nombre { get; set; }
        public string Apellido { get; set; }

        public string NIT { get; set; }
        public string NombreComercial { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public Int32 IDEmpresa { get; set; }
        public ClienteModel()
        {

        }
    }
}
