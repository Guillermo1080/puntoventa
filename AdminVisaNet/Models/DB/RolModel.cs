﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.DB
{
    [Table("Roll")]
    public class RolModel
    {
        [Key]
        public Int32 IDRoll { get; set; }
        public string Nombre { get; set; }
        public DateTime? FechaCreacion  { get; set; }
        public Int32 IDEstado { get; set; }
        public Int32 IDEmpresa { get; set; }
        public RolModel()
        {

        }
    }
}
