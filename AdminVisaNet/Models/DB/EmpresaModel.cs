﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.DB
{
    [Table("Empresa")]
    public class EmpresaModel
    {
        [Key]
        public Int32 IDEmpresa { get; set; }

        public string RazonSocial { get; set; }

        public string NIT { get; set; }

        public string Direccion { get; set; }
        public DateTime? FechaCreacion { get; set; }

        public Int32 IDEstado { get; set; }

        public EmpresaModel()
        {

        }

    }
}
