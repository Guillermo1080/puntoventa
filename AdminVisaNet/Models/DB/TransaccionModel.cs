﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.DB
{
    [Table("transaccion")]
    public class TransaccionModel
    {
        [Key]
        public Int32 IDTransaccion { get; set; }
        public Int32 IDEstablecimiento { get; set; }
        public DateTime? FechaEmision { get; set; }
        public Int32 IDBitacoraTransaccion { get; set; }
        public decimal Monto { get; set; }
        public decimal MontoIVA { get; set; }
        public decimal Descuento { get; set; }
        public Int32 IDCorte { get; set; }
        public Int32 IDUsuario { get; set; }
        public string Observaciones { get; set; }
        public Int32 IDCliente { get; set; }
        public Int32 IDEmpresa { get; set; }
        public string NitReceptor { get; set; }
        public string NombreReceptor { get; set; }
        public Int32 IDTipoVenta { get; set; }
        public string APPRequest { get; set; }

        

        public TransaccionModel()
        {
        }
    }
}
