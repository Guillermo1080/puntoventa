﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.DB
{
    [Table("corte")]
    public class CorteModel
    {
        [Key]
        public Int32 IDCorte { get; set; }
        public Int32 IDEstablecimiento { get; set; }
        public Int32 IDUsuario { get; set; }
        public DateTime? FechaApertura { get; set; }
        public DateTime? FechaCierre { get; set; }
        public Int32 IDEstado { get; set; }        
        public string Observaciones { get; set; }        
        
        public Int32 IDEmpresa { get; set; }
        public CorteModel()
        {

        }
    }
}
