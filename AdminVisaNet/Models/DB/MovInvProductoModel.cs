﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POSWebOS.Models.DB
{
    [Table("MovInvProducto")]
    public class MovInvProductoModel
    {
        [Key]
        public Int32 IDMovInvProducto { get; set; }

        public DateTime? Fecha { get; set; }
        public string Documento { get; set; }
        public decimal Cantidad { get; set; }
        public decimal PrecioUnitario { get; set; }
        public string IDProducto { get; set; }
        public string Observaciones { get; set; }
        public Int32 IDCorte { get; set; }
        public Int32 IDTipoMovProducto { get; set; }
        public Int32 IDTransaccion { get; set; }
        public Int32 IDUsuario { get; set; }
        public Int32 IDProveedor { get; set; }

        
        public MovInvProductoModel()
        {
        }
    }
}
