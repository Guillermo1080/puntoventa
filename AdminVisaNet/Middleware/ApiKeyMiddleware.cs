﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminVisaNet.Middleware
{
    public class ApiKeyMiddleware
    {
        private readonly RequestDelegate _next;
        private const string APIKEYNAME = "ApiKey";
        public ApiKeyMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            string valor = "";
            string APIKEY=null;
            try
            {
                valor = context.GetRouteValue("controller").ToString();
                APIKEY = context.Session.GetString("appKey");
            }
            catch (Exception ex) {
                Console.WriteLine("ERROR en sesiones " + ex.ToString());
            }
            
            
            if (!context.Request.Headers.TryGetValue(APIKEYNAME, out var extractedApiKey))
            {
                if (valor != "Login" && APIKEY==null ) {
                    context.Response.StatusCode = 401;
                    //return RedirectToAction("IndexError", "Home");

                    //context.Response.Redirect("/Login/IndexLogin", false);

                    await context.Response.WriteAsync("Debe iniciar sesion para visualizar la pagina");
                    return;
                }
                
            }

            var configuration = context.RequestServices.GetRequiredService<IConfiguration>();
            var apikey = configuration.GetValue<string>(APIKEYNAME);

            if (!apikey.Equals(APIKEY))
            {
                if (valor != "Login")
                {
                    context.Response.StatusCode = 401;
                    await context.Response.WriteAsync("Cliente no autorizado");
                    //context.Response.Redirect("/Login/IndexLogin", false);
                    return;
                }
                
            }

            await _next(context);
        }

        //public ApiKeyMiddleware()
        //{
        //}
    }
}
